---@diagnostic disable
local xplr = xplr -- The globally exposed configuration to be overridden.
---@diagnostic enable
xplr.config.modes.custom.fzxplr = {
	name = "fzxplr",
	key_bindings = {
		on_key = {
			F = {
				help = "search",
				messages = {
					{
						BashExec = [===[
              PTH=$(cat "${XPLR_PIPE_DIRECTORY_NODES_OUT:?}" | awk -F/ '{print $NF}' | fzf)
              if [ -d "$PTH" ]; then
                "$XPLR" -m 'ChangeDirectory: %q' "$PTH"
              else
                "$XPLR" -m 'FocusPath: %q' "$PTH"
              fi
            ]===],
					},
					"PopMode",
				},
			},
		},
		default = {
			messages = {
				"PopMode",
			},
		},
	},
}

xplr.config.modes.custom.open = {
	name = "open",
	key_bindings = {
		on_key = {
			enter = {
				help = "default open",
				messages = {
					{
						BashExec = [===[
              if [ -z "$(cat $XPLR_PIPE_SELECTION_OUT)" ]; then
                xdg-open $XPLR_FOCUS_PATH || open "$XPLR_FOCUS_PATH" &
              else
                if [ "$(uname)" = "Darwin" ]; then
                  files="open "
                else
                  files="xdg-open "
                fi
                files+=$(sed "s/^\\(.*\\)$/'\\1'/g" < "$XPLR_PIPE_SELECTION_OUT" | paste -sd ' ' -)
                eval "$files"
              fi
            ]===],
					},
					"PopMode",
				},
			},
			e = {
				help = "in nvim",
				messages = {
					{
						BashExec = [===[
              if [ -z "$(cat $XPLR_PIPE_SELECTION_OUT)" ]; then
                nvim $XPLR_FOCUS_PATH
              else
                # cna't use nvim -O $( < files ), because it fails on spaces in file names
                # also can't use while read line; do lines+=("$line"); done < $file; nvim -O $lines as it only works in ZSH
                files="nvim -O "
                files+=$(sed "s/^\\(.*\\)$/'\\1'/g" < "$XPLR_PIPE_SELECTION_OUT" | paste -sd ' ' -)
                eval "$files"
              fi
            ]===],
					},
					"PopMode",
				},
			},
			m = {
				help = "in mpv",
				messages = {
					{
						BashExec = [===[
              mpv $XPLR_FOCUS_PATH & disown
            ]===],
					},
					"PopMode",
				},
			},
			f = {
				help = "in feh",
				messages = {
					{
						BashExec = [===[
              if [ -z "$(cat $XPLR_PIPE_SELECTION_OUT)" ]; then
                feh --auto-zoom --scale-down --draw-filename --draw-tinted --start-at $XPLR_FOCUS_PATH & disown
              else
                files="feh --auto-zoom --scale-down --draw-filename --draw-tinted --start-at "
                files+=$(sed "s/^\\(.*\\)$/'\\1'/g" < "$XPLR_PIPE_SELECTION_OUT" | paste -sd ' ' -)
                eval "$files" & disown
              fi
            ]===],
					},
					"PopMode",
				},
			},
			b = {
				help = "in firefox",
				messages = {
					{
						BashExec = [===[
              firefox "file://$XPLR_FOCUS_PATH" &
            ]===],
					},
					"PopMode",
				},
			},
		},
		default = {
			messages = {
				"PopMode",
			},
		},
	},
}

xplr.config.modes.builtin.default.key_bindings.on_key["F"] = {
	help = "fzf mode",
	messages = {
		{ SwitchModeCustom = "fzxplr" },
	},
}

xplr.config.modes.builtin.default.key_bindings.on_key["enter"] = {
	help = "open mode",
	messages = {
		{ SwitchModeCustom = "open" },
	},
}
