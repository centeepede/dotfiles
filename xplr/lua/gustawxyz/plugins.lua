local M = {}

M.plugins = {
	"dtomvan/xpm.xplr",
	{ name = "sayanarijit/fzf.xplr" },
	{ name = "sayanarijit/regex-search.xplr" },
	{ name = "sayanarijit/map.xplr" },
	{ name = "gitlab:hartan/web-devicons.xplr" },
	-- { name = "prncss-xyz/icons.xplr" },
	-- { name = "dtomvan/extra-icons.xplr" },
	{
		name = "dtomvan/ouch.xplr",
		setup = function()
			require("gustawxyz.configs.ouch")
		end,
	},
	-- {
	-- 	name = "sayanarijit/scp.xplr",
	-- 	setup = function()
	-- 		require("gustawxyz.configs.scp")
	-- 	end,
	-- },
	{ name = "sayanarijit/alacritty.xplr" },
	-- {
	-- 	name = "Junker/nuke.xplr",
	-- 	setup = function()
	-- 		require("gustawxyz.configs.nuke")
	-- 	end,
	-- },
	-- {
	-- 	name = "sayanarijit/command-mode.xplr",
	-- 	setup = function()
	-- 		require("gustawxyz.configs.command-mode")
	-- 	end,
	-- },
}

return M
