require("ouch").setup({
	mode = "action",
	key = "o",
})

local key = xplr.config.modes.builtin.default.key_bindings.on_key

key.o = {
	help = "ouch",
	messages = {
		"PopMode",
		{ SwitchModeCustom = "ouch" },
	},
}
