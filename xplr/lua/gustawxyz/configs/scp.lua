require("scp").setup({
	mode = "selection_ops", -- or `xplr.config.modes.builtin.selection_ops`
	key = "S",
	scp_command = "rsync -Purz",
	non_interactive = false,
	keep_selection = false,
})

-- Type `:sS` and send the selected files.
-- Make sure `~/.ssh/config` or `/etc/ssh/ssh_config` is updated.
-- Else you'll need to enter each host manually.
--
local key = xplr.config.modes.builtin.default.key_bindings.on_key

key.S = {
    help = "send via scp",
    messages = {
      "PopMode",
      { CallLuaSilently = "custom.scp.init" },
    },
  }
