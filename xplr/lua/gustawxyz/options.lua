---@diagnostic disable
local xplr = xplr -- The globally exposed configuration to be overridden.
---@diagnostic enable
--
xplr.config.general.enable_mouse = true

local function black(x)
	return "\x1b[30m" .. x .. "\x1b[0m"
end

local function red(x)
	return "\x1b[31m" .. x .. "\x1b[0m"
end

local function green(x)
	return "\x1b[32m" .. x .. "\x1b[0m"
end

local function yellow(x)
	return "\x1b[33m" .. x .. "\x1b[0m"
end

local function blue(x)
	return "\x1b[34m" .. x .. "\x1b[0m"
end

local function magenta(x)
	return "\x1b[35m" .. x .. "\x1b[0m"
end

local function cyan(x)
	return "\x1b[36m" .. x .. "\x1b[0m"
end

local function white(x)
	return "\x1b[37m" .. x .. "\x1b[0m"
end
xplr.config.node_types.directory.meta.icon = blue("󰉋")
xplr.config.node_types.file.meta.icon = "󰈔"
xplr.config.node_types.symlink.meta.icon = cyan("")

xplr.config.node_types.special["Downloads"] = { meta = { icon = red("󰇚") }, style = { fg = "Red" } }
xplr.config.node_types.special["Documents"] = { meta = { icon = red("󱉟") }, style = { fg = "Red" } }
xplr.config.node_types.special["Pictures"] = { meta = { icon = red("") }, style = { fg = "Red" } }
xplr.config.node_types.special["Videos"] = { meta = { icon = red("󰕧") }, style = { fg = "Red" } }
xplr.config.node_types.special["Movies"] = { meta = { icon = red("󰨜") }, style = { fg = "Red" } }
xplr.config.node_types.special["Music"] = { meta = { icon = red("󰎄") }, style = { fg = "Red" } }
xplr.config.node_types.special["Notes"] = { meta = { icon = red("󰴒") }, style = { fg = "Red" } }
