# Mount at login (what worked)

For the most part this worked with a minor modification
https://wiki.archlinux.org/title/Dm-crypt/Mounting_at_login

This This additional session line is the only modification:
```
/etc/pam.d/system-login
+ auth       optional   pam_exec.so expose_authtok /etc/pam_cryptsetup.sh
+ session   optional  pam_exec.so quiet /etc/pam_cryptsetup.sh
```

```
echo "#!/bin/sh

CRYPT_USER="gustaw"
PARTITION="/dev/sdc"
NAME="home-$CRYPT_USER"

if [ "$PAM_USER" = "$CRYPT_USER" ] && [ ! -e "/dev/mapper/$NAME" ]; then
    /usr/bin/cryptsetup open "$PARTITION" "$NAME"
fi" > /etc/pam_cryptsetup.sh
```

```
echo "[Unit]
Requires=user@1000.service
Before=user@1000.service

[Mount]
Where=/home/gustaw
What=/dev/mapper/home-gustaw
Type=btrfs
Options=defaults,rw,relatime,ssd,space_cache=v2,compress=lzo,subvol=home-gustaw

[Install]
RequiredBy=user@1000.service" > /etc/systemd/system/home-gustaw.mount
```

```
echo "[Unit]
DefaultDependencies=no
BindsTo=dev-sdc.device
After=dev-sdc.device
BindsTo=dev-mapper-home\x2dgustaw.device
Requires=home-gustaw.mount
Before=home-gustaw.mount
Conflicts=umount.target
Before=umount.target

[Service]
Type=oneshot
RemainAfterExit=yes
TimeoutSec=0
ExecStop=/usr/bin/cryptsetup close home-gustaw

[Install]
RequiredBy=dev-mapper-home\x2dgustaw.device" > /etc/systemd/system/cryptsetup-gustaw.service
```
