from libqtile.command import lazy
from libqtile.backend.base.window import Window
# from libqtile.command_client import InteractiveCommandClient


class Functions:

    ##### MOVE WINDOW IN GROUPS #####
    @staticmethod
    def window_to_prev_group():
        @lazy.function
        def __inner(qtile):
            i = qtile.groups.index(qtile.current_group)

            if qtile.current_window and i != 0:
                group = qtile.groups[i - 1].name
                qtile.current_window.togroup(group, switch_group=True)

        return __inner

    @staticmethod
    def window_to_next_group():
        @lazy.function
        def __inner(qtile):
            i = qtile.groups.index(qtile.current_group)

            if qtile.current_window and i != len(qtile.groups):
                group = qtile.groups[i + 1].name
                qtile.current_window.togroup(group, switch_group=True)

        return __inner


    ##### SWITCH GROUPS WHILE SETTING CORRECT LAYOUT #####
    @staticmethod
    def switch_group_and_change_layout(group_name):
        @lazy.function
        def __inner(qtile):
            is_vertical = qtile.current_screen.info()["width"] > qtile.current_screen.info()["height"]
            layout_index = "monadtall" if is_vertical else "monadwide"
            for g in qtile.groups:
                if g.name == group_name:
                    g.toscreen()
                    g.setlayout(layout_index)
                    return
        return __inner

    ##### MOVE WINDOW THROUGH SCREENS #####
    @staticmethod
    def window_to_previous_screen():
        @lazy.function
        def __inner(qtile):
            i = qtile.screens.index(qtile.current_screen)
            if i != 0:
                group = qtile.screens[i - 1].group.name
                qtile.current_window.togroup(group, switch_group=False)
                qtile.to_screen(i - 1)

        return __inner

    @staticmethod
    def window_to_next_screen():
        @lazy.function
        def __inner(qtile):
            i = qtile.screens.index(qtile.current_screen)
            if i + 1 != len(qtile.screens):
                group = qtile.screens[i + 1].group.name
                qtile.current_window.togroup(group, switch_group=False)
                qtile.to_screen(i + 1)

        return __inner

    ##### KILL ALL WINDOWS #####
    @staticmethod
    def kill_all_windows():
        @lazy.function
        def __inner(qtile):
            for window in qtile.current_group.windows:
                window.kill()

        return __inner

    @staticmethod
    def kill_all_windows_minus_current():
        @lazy.function
        def __inner(qtile):
            for window in qtile.current_group.windows:
                if window != qtile.current_window:
                    window.kill()

        return __inner

    @staticmethod
    def spawn_or_focus(app):
        return Functions.spawn_or_focus_class(app, app)

    @staticmethod
    def spawn_or_focus_class(app, app_cls):
        @lazy.function
        def __inner(qtile):
            """Check if the app being launched is already running, if so focus it"""
            window = None
            for win in qtile.windows_map.values():
                if isinstance(win, Window):
                    wm_class: list | None = win.get_wm_class()
                    if wm_class is None or win.group is None:
                        return
                    if any(item.lower() in app_cls.lower() for item in wm_class):
                        window = win
                        group = win.group
                        group.toscreen(toggle=False)
                        break

            if window is None:
                qtile.spawn(app)

            elif window == qtile.current_window:
                try:
                    assert (
                        qtile.current_layout.swap_main is not None
                    ), "The current layout should have swap_main"
                    qtile.current_layout.swap_main()
                except AttributeError:
                    return
            else:
                qtile.current_group.focus(window)
        return __inner



class PWA:
    def __init__(self):
        pass

    @staticmethod
    def spotify():
        return "firefox --new-window https://open.spotify.com/"

if __name__ == "__main__":
    print("This is an utilities module")
