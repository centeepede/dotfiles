from libqtile import layout


class Layouts:
    def __init__(self):
        self.default = {
            "ratio": 0.6,
            "max_ratio": 0.8,
            "min_ratio": 0.2,
            "border_width": 2,
            "margin": 0,
            "border_focus": "#668bd7",
            "border_normal": "1D2330",
            "new_client_position": "before_current",
            # "Place new windows: "
            # " after_current - after the active window."
            # " before_current - before the active window,"
            # " top - at the top of the stack,"
            # " bottom - at the bottom of the stack,",
        }

    def init_layouts(self):
        """
        Returns the layouts variable
        """
        layouts = [
            layout.MonadTall(**self.default),
            layout.MonadWide(**self.default),
            # layout.RatioTile(**self.default),
            layout.Zoomy(),
            # layout.TreeTab(
            #     font="Ubuntu",
            #     fontsize=10,
            #     sections=["FIRST", "SECOND", "THIRD", "FOURTH"],
            #     section_fontsize=10,
            #     border_width=2,
            #     bg_color="1c1f24",
            #     active_bg="c678dd",
            #     active_fg="000000",
            #     inactive_bg="a9a1e1",
            #     inactive_fg="1c1f24",
            #     padding_left=0,
            #     padding_x=0,
            #     padding_y=5,
            #     section_top=10,
            #     section_bottom=20,
            #     level_shift=8,
            #     vspace=3,
            #     panel_width=100,
            # ),
            # layout.Max(**self.default),
            # layout.floating.Floating(**self.default),
            # layout.Stack(num_stacks=2),
            # layout.Bsp(),
            # layout.Columns(),
            # layout.Matrix(),
            # layout.Tile(),
            # layout.VerticalTile(),
        ]
        return layouts
