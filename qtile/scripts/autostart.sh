#!/bin/bash

function run {
    if ! pgrep $1 > /dev/null ;
    then
        $@&
    fi
}
# run "dex $HOME/.config/autostart/arcolinux-welcome-app.desktop"
#run "xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal"
#run "xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off"
run xrandr --output HDMI-A-0 --set TearFree on; xrandr --output DVI-D-0 --set TearFree on; xrandr --output DVI-D-1 --set TearFree on;

run xfsettingsd
run thunar --daemon
run emacs --daemon
run xfce4-power-manager
run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
run nextcloud --background

run light-locker --lock-on-suspend --no-late-locking
run picom -b --config ~/.config/qtile/scripts/picom.conf
run flameshot
input-remapper-control --command stop-all && input-remapper-control --command autoload

run kdeconnect-indicator
# run syncthing-gtk -m
run nm-applet
run pa-applet


# run /usr/local/bin/usb-deft-pro-remap.sh
run numlockx on
# run setxkbmap -option caps:swapescape

/usr/bin/feh --bg-fill /usr/share/backgrounds/arco-login.jpg &

#run applications from startup
run element-desktop
# run "turn-avedon ON"
#
run bash ~/.screenlayout/main.sh
