import re

from icons import group_icons
from libqtile.config import Group, Match


class CreateGroups:
    def init_groups(self):
        """
        Return the groups of Qtile
        """
        groups = [
            Group(
                group_icons["1"],
                position=0,
                # init=False,
                # persist=False,
                matches=[Match(wm_class="firefox")],
                layout="monadtall",
            ),
            Group(
                group_icons["2"],
                position=1,
                # init=False,
                # persist=False,
                matches=[
                    Match(wm_class="Emacs"),
                    Match(wm_class="Alacritty"),
                ],
                layout="monadtall",
            ),
            Group(
                group_icons["3"],
                position=2,
                # init=False,
                # persist=False,
                matches=[
                    Match(wm_class="Ranger"),
                    Match(wm_class="Thunar"),
                    Match(wm_class="Gthumb"),
                    Match(wm_class="qbittorrent"),
                ],
                layout="monadtall",
            ),
            Group(
                group_icons["4"],
                position=3,
                # init=False,
                # persist=False,
                matches=[
                    Match(wm_class="kdenlive"),
                    Match(wm_class="Cura"),
                    Match(wm_class="Blender"),
                    Match(wm_class="gimp"),
                    Match(wm_class="vlc"),
                    Match(wm_class="mpv"),
                    Match(wm_class="freetube"),
                ],
                layout="monadtall",
            ),
            Group(
                group_icons["5"],
                position=4,
                layout="monadtall",
                matches=[
                    Match(wm_class="Pavucontrol"),
                    Match(wm_class="easyeffects"),
                ],
            ),
            Group(
                group_icons["6"],
                position=5,
                layout="monadtall",
                matches=[
                    Match(title=re.compile(r"Minecraft.*")),
                    Match(title=re.compile(r"MultiMC \d.*")),
                ],
            ),
            Group(group_icons["7"], position=6, layout="monadtall"),
            Group(
                group_icons["8"],
                position=7,
                layout="monadtall",
                matches=[
                    Match(wm_class="Logseq"),
                ],
            ),
            Group(
                group_icons["9"],
                position=8,
                # init=False,
                # persist=False,
                matches=[Match(wm_class="Element"), Match(wm_class="thunderbird")],
                layout="monadtall",
            ),
        ]
        return groups
