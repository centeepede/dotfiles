#!python3

import ziafont
font = ziafont.Font('SymbolsNerdFontMono-Regular.ttf')
'./'
battery = {
'battery-caution-charging.png': '󰂃󱐋',
'battery-caution.png': '󰂃',
'battery-empty.png': '󱉝',
'battery-full-charged.png': '󰁹',
'battery-full-charging.png': '󰁹󱐋',
'battery-full.png': '󰁹',
'battery-good-charging.png': '󰁿󱐋',
'battery-good.png': '󰁿',
'battery-low-charging.png': '󰁺󱐋',
'battery-low.png': '󰁺',
'battery-missing.png': '󱉝',
}
for i, v in battery.items():
    with open(i[:-3]+"svg", "w") as file:
        file.write(font.text(v).svg())
