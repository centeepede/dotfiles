###############################################
# Gustaw.xyz config
####### IMPORTS #########
import os
import subprocess

from groups import CreateGroups
from icons import group_icons

# Local Files
from keys import Keybindings, Mouse
from layouts import Layouts
from libqtile import hook, layout
from libqtile.command import lazy
from libqtile.config import Match, Rule
from libqtile.log_utils import logger
from widgets import MyWidgets

# from typing import List  # noqa: F401


###### MAIN ######
if __name__ in ["config", "__main__"]:
    obj_keys = Keybindings()
    obj_mouse = Mouse()
    obj_widgets = MyWidgets()
    obj_layouts = Layouts()
    obj_groups = CreateGroups()

    # Initializes qtile variables
    keys = obj_keys.init_keys()
    mouse = obj_mouse.init_mouse()
    layouts = obj_layouts.init_layouts()
    groups = obj_groups.init_groups()

    # Append group keys for groups
    keys += obj_keys.init_keys_groups(group_icons)

    ### DISPLAYS WIDGETS IN THE SCREEN ####

    screens = obj_widgets.init_screen()

from libqtile.backend.wayland import InputConfig

wl_input_rules = {
        "*": InputConfig(kb_layout="pl"),
        # "1267:12377:ELAN1300:00 04F3:3059 Touchpad": InputConfig(left_handed=True),
        # "type:keyboard": InputConfig(kb_options="ctrl:nocaps,compose:ralt"),
}

dgroups_key_binder = None
dgroups_app_rules = [
    # Everything i want to be float, but don't want to change group
    Rule(Match(wm_class="flameshot"), float=False, intrusive=True),
    # floating windows
    # Rule(Match(wm_class=['Synfigstudio', 'Wine', 'Xephyr', 'postal2-bin'],
    #         # title=[re.compile('[a-zA-Z]*? Steam'),
    #         #        re.compile('Steam - [a-zA-Z]*?')]
    #         ),
    #     float=True),
    # static windows (unmanaged)
    # Rule(Match(wm_class=["XTerm"]), static=True),
    # Rule(Match(net_wm_pid=["XTerm"]), float=True),
]

follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False


floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        # *layout.Floating.default_float_rules,
        Match(wm_type="utility"),
        Match(wm_type="notification"),
        Match(wm_type="toolbar"),
        Match(wm_type="splash"),
        Match(wm_type="dialog"),
        Match(wm_class="file_progress"),
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="notification"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="dialog"),  # Dialogs stuff
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_fullscreen = True
# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
respect_minimize_requests = False
auto_minimize = False

# Gasp! We're lying here. In fact, nobody really uses or cares about this
wmname = "LG3D"


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.local/bin/autostart"])


@hook.subscribe.client_new
def dialogs(window):
    if window.window.get_wm_type() == "dialog" or window.window.get_wm_transient_for():
        window.floating = True


# Focus new window
@hook.subscribe.client_managed
def func(client):
    logger.warning(client.info()["wm_class"])
    if client.group != lazy.group:
        client.group.toscreen()
