import os
from libqtile import bar, widget
from libqtile.lazy import lazy
from libqtile.config import Screen

from functions import PWA
# widget_defaults = dict(
#     font="Ubuntu Mono",
#     fontsize = 12,
#     padding = 2,
#     background=colors[2]
# )

# extension_defaults = widget_defaults.copy()


class MyWidgets:
    def __init__(self):
        self.colors = [["#292d3e", "#292d3e"],  # panel background
                       # background for current screen tab
                       ["#434758", "#434758"],
                       ["#ffffff", "#ffffff"],  # font color for group names
                       # border line color for current tab
                       ["#bc13fe", "#bc13fe"],  # Group down color
                       # border line color for other tab and odd widgets
                       ["#8d62a9", "#8d62a9"],
                       ["#668bd7", "#668bd7"],  # color for the even widgets
                       ["#e1acff", "#e1acff"],  # window name

                       ["#000000", "#000000"],
                       ["#AD343E", "#AD343E"],
                       ["#f76e5c", "#f76e5c"],
                       ["#F39C12", "#F39C12"],
                       ["#F7DC6F", "#F7DC6F"],
                       ["#f1ffff", "#f1ffff"],
                       ["#4c566a", "#4c566a"], ]

        self.termite = "alacritty"

    def init_widgets_list(self, has_systray, size_increase = 0):
        '''
        Function that returns the desired widgets in form of list
        '''
        widgets_list = []
        widgets_list += [
            widget.Sep(
                linewidth=0,
                padding=6,
                foreground=self.colors[2],
                background=self.colors[0]
            ),
            widget.Image(
                filename="~/.config/qtile/icons/terminal-iconx14.png",
                mouse_callbacks={
                    'Button1': lambda lazy: lazy.spawncmd(f"dmenu_run -i -fn 'Roboto Mono:12' -nb '{self.colors[0]}' -nf '{self.colors[6]}' -sb '{self.colors[9]}' -sf '{self.colors[2]}'")}
            ),
            widget.Sep(
                linewidth=0,
                padding=5,
                foreground=self.colors[2],
                background=self.colors[0]
            ),
            widget.GroupBox(
                font="Ubuntu Bold",
                fontsize=12,
                margin_y=2,
                margin_x=0,
                padding_y=5,
                padding_x=3,
                borderwidth=3,
                active=self.colors[-2],
                inactive=self.colors[-1],
                # rounded=True,
                rounded=False,
                # highlight_color=self.colors[9],
                # highlight_method="line",
                highlight_method='block',
                urgent_alert_method='block',
                # urgent_border=self.colors[9],
                this_current_screen_border=self.colors[9],
                this_screen_border=self.colors[4],
                other_current_screen_border=self.colors[0],
                other_screen_border=self.colors[0],
                foreground=self.colors[2],
                background=self.colors[0],
                disable_drag=True
            ),
            widget.Prompt(
                prompt=lazy.spawncmd(),
                font="Ubuntu Mono",
                padding=10,
                foreground=self.colors[3],
                background=self.colors[1]
            ),
            widget.Sep(
                linewidth=0,
                padding=40,
                foreground=self.colors[2],
                background=self.colors[0]
            ),
            widget.TaskList(
                foreground=self.colors[6],
                background=self.colors[0],
                border=self.colors[1],
                icon_size=18,
                max_title_width= 120,
                highlight_method='block',
                markup_floating='~{}',
                markup_focused='*{}',
                markup_minimized='_{}',
                markup_normal='{}',
                borderwidth=3,
                padding=0
            ),
        ]
        if has_systray:
            widgets_list += [
                widget.Systray(
                    background=self.colors[0],
                    padding=5
                ),
            ]
        widgets_list += [
            widget.TextBox(
                text='',
                background=self.colors[0],
                foreground=self.colors[10],
                padding=0,
                fontsize=37+size_increase*4
            ),
            widget.TextBox(
                text="󰆓 ",
                foreground=self.colors[7],
                background=self.colors[10],
                padding=0,
                fontsize=15
            ),
            widget.Memory(
                foreground=self.colors[7],
                background=self.colors[10],
                measure_mem='G',
                mouse_callbacks={'Button1': lazy.spawn(
                    self.termite + ' -e gtop')},
                padding=5
            ),
            widget.TextBox(
                text='',
                background=self.colors[10],
                foreground=self.colors[11],
                padding=0,
                fontsize=37+size_increase*4
            ),
        ]
        # Add battery only if it's present and doesn't error out
        try:
            widget.Battery(
                    foreground=self.colors[7],
                    background=self.colors[11],
                    mouse_callbacks={'Button1': lazy.spawn('xfce4-power-manager-settings')},
                    padding=0,
                    # format='{char} {percent:2.0%}|{hour:d}:{min:02d}|{watt:.0f}W',
                    format='{percent:2.0%}|{watt:.0f}W',
                )._battery.update_status()
            widgets_list += [
                widget.BatteryIcon(
                    theme_path=os.path.expanduser(
                        "~/.config/qtile/icons/battery-icons/"),
                    foreground=self.colors[7],
                    background=self.colors[11],
                    mouse_callbacks={'Button1': lazy.spawn('xfce4-power-manager-settings')},
                    padding=0,
                ),
                widget.Battery(
                    foreground=self.colors[7],
                    background=self.colors[11],
                    mouse_callbacks={'Button1': lazy.spawn('xfce4-power-manager-settings')},
                    padding=0,
                    # format='{char} {percent:2.0%}|{hour:d}:{min:02d}|{watt:.0f}W',
                    format='{percent:2.0%}|{hour:d}:{min:02d}|{watt:.0f}W',
                ),
            ]
        except RuntimeError:
            pass
        widgets_list += [
            widget.TextBox(
                text='',
                background=self.colors[11],
                foreground=self.colors[10],
                padding=0,
                fontsize=37+size_increase*4
            ),
            widget.TextBox(
                text="  ",
                foreground=self.colors[7],
                background=self.colors[10],
                padding=0,
                mouse_callbacks={
                    "Button1": lazy.spawn('pavucontrol')}
            ),
            widget.Volume(
                foreground=self.colors[7],
                background=self.colors[10],
                padding=5
            ),
            widget.TextBox(
                text='',
                background=self.colors[10],
                foreground=self.colors[9],
                padding=0,
                fontsize=37+size_increase*4
            ),
            widget.CurrentLayoutIcon(
                custom_icon_paths=[os.path.expanduser(
                    "~/.config/qtile/icons")],
                foreground=self.colors[0],
                background=self.colors[9],
                padding=0,
                scale=0.7
            ),
            # widget.CurrentLayout(
            #     foreground=self.colors[7],
            #     background=self.colors[9],
            #     padding=5
            # ),
            widget.TextBox(
                text='',
                foreground=self.colors[8],
                background=self.colors[9],
                padding=0,
                fontsize=37+size_increase*4
            ),
            widget.Clock(
                foreground=self.colors[7],
                background=self.colors[8],
                mouse_callbacks={
                    "Button1": lazy.spawn(PWA.spotify())},
                format="%d %B [ %H:%M ]"
            ),
            widget.Sep(
                linewidth=0,
                padding=10,
                foreground=self.colors[0],
                background=self.colors[8]
            ),
        ]
        return widgets_list

    def init_widgets_screen(self):
        '''
        Function that returns the widgets in a list.
        It can be modified so it is useful if you  have a multimonitor system
        '''
        widgets_screen = self.init_widgets_list(True, 0)
        return widgets_screen

    def init_widgets_screen2(self):
        '''
        Function that returns the widgets in a list.
        It can be modified so it is useful if you  have a multimonitor system
        '''
        widgets_screen = self.init_widgets_list(False, 2)
        return widgets_screen

    def init_screen(self):
        '''
        Init the widgets in the screen
        '''
        screen_args = {
            # 'wallpaper': '/usr/share/backgrounds/arco-login.jpg',
        }
        screens = []
        screens.append(
            Screen(top=bar.Bar(widgets=self.init_widgets_screen(), size=20, opacity = 1.0, size_increase=4),
                   **screen_args)) # type: ignore
        for _ in range(2):
            screens.append(
                Screen(top=bar.Bar(widgets=self.init_widgets_screen2(), size=22, opacity = 1.0, size_increase=4),
                       **screen_args)) # type: ignore
        # for screen in screens:
        #     is_vertical = screen.info()["width"] > screen.info()["height"]
        #     layout_index = "monadtall" if is_vertical else "monadwide"
            # screen.group.cmd_setlayout(layout_index)
        return screens
