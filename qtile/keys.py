from os.path import expanduser

from libqtile.lazy import lazy
from libqtile.config import Click, Drag, Key
from libqtile import extension

from functions import Functions, PWA
from widgets import MyWidgets


HOME = expanduser("~")
TERMINAL = "alacritty"

bg_normal = MyWidgets().colors[0][0]
fg_normal = MyWidgets().colors[2][0]
bg_focus = MyWidgets().colors[9][0]
fg_focus = MyWidgets().colors[2][0]

############       MODIFIERS            ##############
MOD = "mod4"
ALT = "mod1"
ALTGR = "mod5"
SHIFT = "shift"
CONTROL = "control"

class Keybindings:
    keys = []

    def generate_keybindings(self):

        self.keys += [
            # Volume
            Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
            Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
            Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
            # Key([], "XF86AudioLowerVolume", lazy.widget["volumectrl"].adjust_volume("decrease")),
            # Key([], "XF86AudioRaiseVolume", lazy.widget["volumectrl"].adjust_volume("increase")),
            # Key([], "XF86AudioMute", lazy.widget["volumectrl"].adjust_volume("mute")),
            # ([], "XF86AudioMicMute", lazy.spawn(" TODO FIX ME")),
            # Media Keys
            Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
            Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
            Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
            # Brightness
            Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%")),
            Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),
            # Run "rofi-theme-selector" in terminal to select a theme
            Key([SHIFT, MOD], "space", lazy.spawn('rofi -modi "drun,run,ssh" -show drun -show-icons')),
            Key([MOD], "Tab", lazy.spawn('rofi -modi "window" -show window -show-icons')),
            Key([MOD], "v", lazy.spawn("pavucontrol")),
            Key([CONTROL, ALTGR], "l", lazy.spawn("light-locker-command -l")),
            Key([CONTROL, ALT], "l", lazy.spawn("light-locker-command -l")),
            # Screenshots
            Key([], "Print", lazy.spawn("screen-capture")),
            Key([SHIFT], "Print", lazy.spawn("screen-capture -f")),
            Key([ALT], "Print", lazy.spawn("screen-capture -w")),
            Key([SHIFT, ALT], "Print", lazy.spawn("screen-capture -f -w")),
            Key([CONTROL], "Print", lazy.spawn("screen-capture -a")),
            Key([SHIFT, CONTROL], "Print", lazy.spawn("screen-capture -f -a")),
            # Power
            Key([MOD, CONTROL], "q", lazy.spawn("light-locker-command -l")),
            Key([MOD, CONTROL], "w", lazy.spawn("poweroff")),
            Key([MOD, CONTROL], "s", lazy.spawn("systemctl suspend")),
            Key([MOD, CONTROL, SHIFT], "w", lazy.spawn("systemctl reboot")),
            Key([MOD, CONTROL, SHIFT], "s", lazy.spawn("systemctl hibernate")),

            Key([MOD], "b", Functions.spawn_or_focus("firefox")),

            Key(
                [MOD, CONTROL],
                "c", lazy.spawn(
                TERMINAL
                + f" -e nvim --cmd 'cd {HOME}/.config/qtile' {HOME}/.config/qtile/keys.py",
            )),
            Key(
                [MOD, SHIFT],
                "f", lazy.spawn(
                TERMINAL
                + f" -e nvim --cmd 'cd {HOME}/Documents/Neorg/' {HOME}/Documents/Neorg/index.norg",
            )),
            Key(
                [MOD],
                "space", lazy.spawn(
                f"dmenu_run -i -fn 'Roboto Mono:12' -nb '{bg_normal}' -nf '{fg_normal}' -sb '{bg_focus}' -sf '{fg_focus}'",
            )),
            Key(
                [MOD], "Return", Functions.spawn_or_focus_class(TERMINAL + " -e zsh -c 'tmux attach || tmux'", TERMINAL)
            ),
            Key(
                [MOD], "e", Functions.spawn_or_focus_class(TERMINAL + " --class Ranger -e ranger", "Ranger")
            ),
            Key([MOD, SHIFT], "e", lazy.spawn("thunar")),
            Key([MOD, ALTGR], "s", lazy.spawn(PWA.spotify())),

            Key([MOD], "h", lazy.layout.left()),
            Key([MOD], "l", lazy.layout.right()),
            Key([MOD], "j", lazy.layout.down()),
            Key([MOD], "k", lazy.layout.up()),

            # Key([MOD], "Tab", lazy.next_layout()),

            Key([MOD, SHIFT], "h", lazy.layout.swap_left()),
            Key([MOD, SHIFT], "l", lazy.layout.swap_right()),
            Key([MOD, SHIFT], "j", lazy.layout.shuffle_down()),
            Key([MOD, SHIFT], "k", lazy.layout.shuffle_up()),

            Key([MOD], "i", lazy.layout.grow()),
            Key([MOD, SHIFT], "i", lazy.layout.shrink()),
            Key([MOD], "o", lazy.layout.normalize()),
            Key([SHIFT, MOD], "o", lazy.layout.reset()),
            Key([MOD], "m", lazy.layout.maximize()),
            Key([MOD], "n", lazy.window.toggle_minimize()),
            Key([CONTROL, MOD], "n", lazy.group.unminimize_all()),

            Key([MOD, SHIFT, CONTROL], "r", lazy.shutdown()),
            Key([MOD, SHIFT], "r", lazy.restart()),
            Key([MOD, CONTROL], "r", lazy.reload_config()),
            Key(
                [MOD, ALTGR], "q", Functions.kill_all_windows_minus_current()
            ),
            Key([MOD, SHIFT, ALTGR], "q", Functions.kill_all_windows()),
            Key([MOD], "q", lazy.window.kill()),
            Key([MOD], "f", lazy.window.toggle_floating()),
            Key([SHIFT, MOD], "m", lazy.window.toggle_fullscreen()),

            Key([CONTROL, MOD], "Right", lazy.screen.next_group()),
            Key([CONTROL, MOD], "Left", lazy.screen.prev_group()),
            Key([SHIFT, CONTROL, MOD], "Right", Functions.window_to_next_group()),
            Key([SHIFT, CONTROL, MOD], "Left", Functions.window_to_prev_group()),
            Key([MOD], "Left", lazy.next_screen()),
            Key([MOD], "Up", lazy.next_screen()),
            Key([MOD], "Right", lazy.prev_screen()),
            Key([MOD], "Down", lazy.prev_screen()),
            Key([SHIFT, MOD], "Right", Functions.window_to_next_screen()),
            Key([SHIFT, MOD], "Down", Functions.window_to_next_screen()),
            Key([SHIFT, MOD], "Left", Functions.window_to_previous_screen()),
            Key([SHIFT, MOD], "Up", Functions.window_to_previous_screen()),

            Key(
                [MOD],
                "w",
                lazy.run_extension(
                    extension.J4DmenuDesktop(
                        dmenu_prompt="APPS:",
                        dmenu_font="Roboto Mono:12",
                        background=bg_normal,
                        foreground=fg_normal,
                        selected_background=bg_focus,
                        selected_foreground=fg_focus,
                        #  dmenu_height=24,  # Only supported by some dmenu forks
                    )
                ),
            ),
        ]

    def init_keys_groups(self, group_names):
        """
        Create bindings to move between groups
        """
        group_keys = []
        for key, name in group_names.items():
            group_keys += [
                Key([MOD], key, Functions.switch_group_and_change_layout(name)),
                Key([MOD, SHIFT], key, lazy.window.togroup(name, switch_group=False)),
            ]

        return group_keys

    def init_keys(self):
        self.generate_keybindings()
        return self.keys


class Mouse:
    def __init__(self, mod_key=MOD):
        self.mod = mod_key

    def init_mouse(self):
        mouse = [
            Drag(
                [self.mod],
                "Button1",
                lazy.window.set_position_floating(),
                start=lazy.window.get_position(),
            ),
            Drag(
                [self.mod],
                "Button3",
                lazy.window.set_size_floating(),
                start=lazy.window.get_size(),
            ),
            Click([self.mod], "Button2", lazy.window.bring_to_front()),
        ]
        return mouse
