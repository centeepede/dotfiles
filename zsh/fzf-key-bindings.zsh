#     ____      ____
#    / __/___  / __/
#   / /_/_  / / /_
#  / __/ / /_/ __/
# /_/   /___/_/ key-bindings.zsh
#
# - $FZF_TMUX_OPTS
# - $FZF_CTRL_T_COMMAND
# - $FZF_CTRL_T_OPTS
# - $FZF_CTRL_R_OPTS
# - $FZF_ALT_C_COMMAND
# - $FZF_ALT_C_OPTS

# eval "$(fasd --init auto)" # Enalbe FASD for zsh completion - Disabled, because unused
# FASD defaults for refference
# alias a='fasd -a'        # any
# alias s='fasd -si'       # show / search / select
# alias d='fasd -d'        # directory
# alias f='fasd -f'        # file
# alias sd='fasd -sid'     # interactive directory selection
# alias sf='fasd -sif'     # interactive file selection
# alias z='fasd_cd -d'     # cd, same functionality as j in autojump

# Key bindings
# ------------

# The code at the top and the bottom of this file is the same as in completion.zsh.
# Refer to that file for explanation.
if 'zmodload' 'zsh/parameter' 2>'/dev/null' && (( ${+options} )); then
    __fzf_key_bindings_options="options=(${(j: :)${(kv)options[@]}})"
else
    () {
        __fzf_key_bindings_options="setopt"
        'local' '__fzf_opt'
        for __fzf_opt in "${(@)${(@f)$(set -o)}%% *}"; do
            if [[ -o "$__fzf_opt" ]]; then
                __fzf_key_bindings_options+=" -o $__fzf_opt"
            else
                __fzf_key_bindings_options+=" +o $__fzf_opt"
            fi
        done
    }
fi

'emulate' 'zsh' '-o' 'no_aliases'

{

    [[ -o interactive ]] || return 0

    # CTRL-T - Paste the selected file path(s) into the command line
    __fsel() {
        local cmd
        if [[ $# -eq 0 ]]; then
            cmd="${FZF_CTRL_T_COMMAND:-"command find -L . -mindepth 1 \\( -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
                -o -type f -print \
                -o -type d -print \
                -o -type l -print 2> /dev/null | cut -b3-"}"
        else
            cmd="${FZF_CTRL_T_COMMAND:-"command find -L . -mindepth 1 \\( -path '$1/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
                -o -type f -print \
                -o -type d -print \
                -o -type l -print 2> /dev/null | cut -b3-"}"
        fi
        setopt localoptions pipefail no_aliases 2> /dev/null
        local item
        eval "$cmd" | FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse --bind=ctrl-z:ignore $FZF_DEFAULT_OPTS $FZF_CTRL_T_OPTS" $(__fzfcmd) | while read item; do
            echo -n "${(q)item} "
        done
        local ret=$?
        echo
        return $ret
    }

    __fzfcmd() {
        [ -n "$TMUX_PANE" ] && { [ "${FZF_TMUX:-0}" != 0 ] || [ -n "$FZF_TMUX_OPTS" ]; } &&
        echo "fzf-tmux ${FZF_TMUX_OPTS:--d${FZF_TMUX_HEIGHT:-40%}} -- " || echo "fzf"
    }

    fzf-file-widget() {
        LBUFFER="${LBUFFER}$(__fsel)"
        local ret=$?
        zle reset-prompt
        return $ret
    }
    zle     -N            fzf-file-widget
    bindkey -M emacs '^T' fzf-file-widget
    bindkey -M vicmd '^T' fzf-file-widget
    bindkey -M viins '^T' fzf-file-widget

    # ALT-C - cd into the selected directory
    fzf-cd-widget() {
        local cmd="${FZF_ALT_C_COMMAND:-"command find -L . -mindepth 1 \\( -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
            -o -type d -print 2> /dev/null | cut -b3-"}"
        setopt localoptions pipefail no_aliases 2> /dev/null
        local dir="$(eval "$cmd" | FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse --bind=ctrl-z:ignore $FZF_DEFAULT_OPTS $FZF_ALT_C_OPTS" $(__fzfcmd) +m)"
        if [[ -z "$dir" ]]; then
            zle redisplay
            return 0
        fi
        zle push-line # Clear buffer. Auto-restored on next prompt.
        BUFFER="builtin cd -- ${(q)dir}"
        zle accept-line
        local ret=$?
        unset dir # ensure this doesn't end up appearing in prompt expansion
        zle reset-prompt
        return $ret
    }
    # zle     -N             fzf-cd-widget
    # bindkey -M emacs '\ec' fzf-cd-widget
    # bindkey -M vicmd '\ec' fzf-cd-widget
    # bindkey -M viins '\ec' fzf-cd-widget

    fzf-push-directory-widget() {
        setopt localoptions pipefail no_aliases 2> /dev/null
        [ $# -gt 0 ] && fasd_cd -d "$*" && return
        local dir="$(fasd -Rdl "$1" | fzf -1 -0 --no-sort +m)" || return 1
        if [[ -z "$dir" ]]; then
            zle redisplay
            return 0
        fi
        zle push-line # Clear buffer. Auto-restored on next prompt.
        BUFFER="builtin pushd -- ${(q)dir}"
        zle accept-line
        local ret=$?
        unset dir # ensure this doesn't end up appearing in prompt expansion
        zle reset-prompt
        return $ret
    }

    zle     -N            fzf-push-directory-widget
    bindkey -M emacs '^p' fzf-push-directory-widget
    bindkey -M vicmd '^p' fzf-push-directory-widget
    bindkey -M viins '^p' fzf-push-directory-widget

    # CTRL-R - Paste the selected command from history into the command line
    fzf-history-widget() {
        local selected num
        setopt localoptions noglobsubst noposixbuiltins pipefail no_aliases 2> /dev/null
        selected=( $(fc -rl 1 | awk '{ cmd=$0; sub(/^[ \t]*[0-9]+\**[ \t]+/, "", cmd); if (!seen[cmd]++) print $0 }' |
        FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} $FZF_DEFAULT_OPTS -n2..,.. --tiebreak=index --bind=ctrl-r:toggle-sort,ctrl-z:ignore $FZF_CTRL_R_OPTS --query=${(qqq)LBUFFER} +m" $(__fzfcmd)) )
        local ret=$?
        if [ -n "$selected" ]; then
            num=$selected[1]
            if [ -n "$num" ]; then
                zle vi-fetch-history -n $num
            fi
        fi
        zle reset-prompt
        return $ret
    }
    zle     -N            fzf-history-widget
    bindkey -M emacs '^R' fzf-history-widget
    bindkey -M vicmd '^R' fzf-history-widget
    bindkey -M viins '^R' fzf-history-widget

    fzf-directory-widget() {
        [ $# -gt 0 ] && fasd_cd -d "$*" && return
        local dir
        dir="$(fasd -Rdl "$1" | fzf -1 -0 --no-sort +m)" && cd "${dir}" || return 1
    }

    zle     -N            fzf-directory-widget
    bindkey -M emacs '^f' fzf-directory-widget
    bindkey -M vicmd '^f' fzf-directory-widget
    bindkey -M viins '^f' fzf-directory-widget

    fzf-edit-widget() {
        [ $# -gt 0 ] && fasd_cd -d "$*" && return
        local dir
        dir="$(fasd -Rdl "$1" | fzf -1 -0 --no-sort +m)" && nvim "${dir}" || return 1
    }

    zle     -N            fzf-edit-widget
    bindkey -M emacs '^e' fzf-edit-widget
    bindkey -M vicmd '^e' fzf-edit-widget
    bindkey -M viins '^e' fzf-edit-widget

    fzf-git-widget() {
        local git_root
        if [[ -z "$(git rev-parse --show-toplevel 2>/dev/null)" ]]; then return 1; fi
        git_root=$(git rev-parse --show-toplevel)
        LBUFFER="${LBUFFER}$(__fsel $git_root)"
        local ret=$?
        zle reset-prompt
        return $ret
    }

    zle     -N            fzf-git-widget
    bindkey -M emacs '^g' fzf-git-widget
    bindkey -M vicmd '^g' fzf-git-widget
    bindkey -M viins '^g' fzf-git-widget

    } always {
    eval $__fzf_key_bindings_options
    'unset' '__fzf_key_bindings_options'

}
