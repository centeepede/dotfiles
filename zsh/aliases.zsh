## Alias section
alias la="ls -Ah --colo=always"
alias ls="ls -h --colo=always"
alias ll="ls -lAh --colo=always"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias df='df -h'                                                # Human-readable sizes
alias du='du -h'                                                # Human-readable sizes
alias free='free -m'                                            # Show sizes in MB
alias xlicp='xclip -sel clip'
alias grep='grep --color'
alias egrep='egrep --color'

_sync-to-vps () {
    # rsync -urvP vpss:/var/www/gustaw $1;
    echo disabled;
}
alias sync-to-vps="_sync-to-vps()"

eval "$(fasd --init posix-alias zsh-hook)"
cdd() {
    [ $# -gt 0 ] && fasd_cd -d "$*" && return
    local dir
    dir="$(fasd -Rdl "$1" | fzf -1 -0 --no-sort +m)" && cd "${dir}" || return 1
}
unalias zz 2> /dev/null
zz() {
    [ $# -gt 0 ] && fasd_cd -d "$*" && return
    local dir
    dir="$(fasd -Rdl "$1" | fzf -1 -0 --no-sort +m)" && pushd "${dir}" || return 1
}
