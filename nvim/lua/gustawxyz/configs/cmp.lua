local cmp = require "cmp"
local luasnip = require "luasnip"

local opts = {
  completion = {
    completeopt = "menu,menuone",
  },
  view = {
    entries = { name = "custom", selection_order = "near_cursor" },
  },
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end,
  },

  mapping = {
    ["<C-Space>"] = cmp.mapping.complete(),
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        local entry = cmp.get_selected_entry()
        if not entry then
          cmp.select_next_item { behavior = cmp.SelectBehavior.Insert }
        else
          cmp.confirm()
        end
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { "i", "s", "c" }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        local entry = cmp.get_selected_entry()
        if not entry then
          cmp.select_next_item { behavior = cmp.SelectBehavior.Insert }
        else
          cmp.confirm()
        end
      elseif require("luasnip").jumpable(-1) then
        vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true), "")
      else
        fallback()
      end
    end, { "i", "s", "c" }),
    ["<Down>"] = cmp.mapping(cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Select, preserve_mapping_verticality = true }, { "i", "s" }),
    ["<Up>"] = cmp.mapping(cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Select, preserve_mapping_verticality = true }, { "i", "s" }),
    ["<S-Down>"] = cmp.mapping(cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Select, preserve_mapping_verticality = true }, { "c" }),
    ["<S-Up>"] = cmp.mapping(cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Select, preserve_mapping_verticality = true }, { "c" }),
  },
  sources = {
    { name = "nvim_lsp" },
    { name = "luasnip" },
    { name = "buffer" },
    { name = "nvim_lua" },
    { name = "path" },
    { name = "crates" },
    { name = "neorg" },
    -- { name = "git" },
    -- { name = "cmdline" },
  },
}

-- configure `cmp-cmdline` as described in their repo: https://github.com/hrsh7th/cmp-cmdline#setup
cmp.setup.cmdline("/", {
  mapping = cmp.mapping.preset.cmdline(opts.mapping),
  sources = {
    { name = "buffer" },
  },
})
cmp.setup.cmdline(":", {
  -- mapping = cmp.mapping.preset.cmdline(),
  mapping = cmp.mapping.preset.cmdline(opts.mapping),
  sources = cmp.config.sources({
    { name = "path" },
  }, {
    {
      name = "cmdline",
      option = {
        ignore_cmds = { "Man", "!" },
      },
    },
  }),
})

-- run cmp setup
cmp.setup(opts)
