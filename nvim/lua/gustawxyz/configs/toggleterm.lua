require("toggleterm").setup{
  autochdir = true, -- when neovim changes it current directory the terminal will change it's own when next it's opened
  start_in_insert = true,
}
require("toggleterm").setup()
