local neogit = require("neogit")

neogit.setup {
  use_magit_keybindings = false,
    integrations = {
    telescope = true,
    diffview = true,
  },
}
