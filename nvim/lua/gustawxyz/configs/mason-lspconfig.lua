local opts = {
  handlers = {
    function(server)
      require("lspconfig")[server].setup {
        capabilities = require("cmp_nvim_lsp").default_capabilities(),
      }
    end,
  },
  ensure_installed = {},
  automatic_installation = true,
}

require("mason-lspconfig").setup(opts)
