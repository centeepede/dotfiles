local capabilities = require("gustawxyz.configs.lspconfig").capabilities


local options = {
  server = {
    capabilities = capabilities,
  }
}

return options

