require("neorg").setup {
  load = {
    ["core.autocommands"] = {},
    ["core.clipboard"] = {},
    ["core.clipboard.code-blocks"] = {},
    ["core.completion"] = {
      config = {
        engine = "nvim-cmp",
      },
    },
    ["core.concealer"] = {},
    ["core.defaults"] = {},
    ["core.dirman.utils"] = {},
    ["core.esupports.hop"] = {},
    ["core.esupports.indent"] = {},
    ["core.esupports.metagen"] = {},
    ["core.fs"] = {},
    ["core.highlights"] = {},
    ["core.integrations.nvim-cmp"] = {},
    ["core.integrations.treesitter"] = {},
    ["core.itero"] = {},
    ["core.journal"] = {},
    ["core.keybinds"] = {},
    ["core.looking-glass"] = {},
    ["core.manoeuvre"] = {},
    ["core.mode"] = {},
    ["core.neorgcmd"] = {},
    ["core.neorgcmd.commands.return"] = {},
    ["core.pivot"] = {},
    ["core.presenter"] = {
      config = {
        zen_mode = "zen-mode",
      },
    },
    ["core.promo"] = {},
    ["core.qol.toc"] = {},
    ["core.qol.todo_items"] = {},
    ["core.queries.native"] = {},
    ["core.scanner"] = {},
    ["core.storage"] = {},
    ["core.summary"] = {},
    ["core.syntax"] = {},
    ["core.tangle"] = {},
    -- ["core.tempus"] = {},
    ["core.ui"] = {},
    -- ["core.ui.calendar"] = {},

    ["core.dirman"] = { -- Manages Neorg workspaces
      config = {
        workspaces = {
          notes = "~/Documents/Neorg",
          dotfiles = "~/.dotfiles/Notes",
        },
        default_workspace = "notes",
      },
    },
    ["core.integrations.telescope"] = {},
    ["core.export"] = {},
    ["core.export.markdown"] = {
      config = {
        extensions = "all",
      },
    },
  },
}

vim.wo.foldlevel = 99
vim.wo.conceallevel = 2

local neorg_callbacks = require "neorg.core.callbacks"

neorg_callbacks.on_event("core.keybinds.events.enable_keybinds", function(_, keybinds)
  -- Map all the below keybinds only when the "norg" mode is active
  keybinds.map_event_to_mode("norg", {
    n = { -- Bind keys in normal mode
      { "<localleader>il", "core.integrations.telescope.find_linkable", opts = { desc = "Find Linkable" } },
      { "<localleader>if", "core.integrations.telescope.find_norg_files", opts = { desc = "Find Neorg Files" } },
      { "<localleader>ff", "core.integrations.telescope.find_norg_files", opts = { desc = "Find Neorg Files" } },
    },

    i = { -- Bind in insert mode
      { "<C-I>", "core.integrations.telescope.insert_link", opts = { desc = "Insert link to Neorg file" } },
      { "<C-i>", "core.integrations.telescope.insert_file_link", opts = { desc = "Find Linkable" } },
    },
  }, {
    silent = true,
    noremap = true,
  })
end)
