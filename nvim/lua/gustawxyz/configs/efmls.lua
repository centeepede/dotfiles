local languages = {
  cmake = {
    require "efmls-configs.formatters.gersemi",
    require "efmls-configs.linters.cmake_lint",
  },
  cpp = {
    require "efmls-configs.formatters.clang_format",
    require "efmls-configs.formatters.clang_tidy",
    require "efmls-configs.linters.clang_tidy",
    require "efmls-configs.linters.cppcheck",
  },
  css = {
    require "efmls-configs.formatters.prettier_d",
    require "efmls-configs.formatters.stylelint",
    require "efmls-configs.linters.stylelint",
  },
  docker = {
    require "efmls-configs.linters.hadolint",
  },
  git = {
    require "efmls-configs.linters.gitlint",
  },
  go = {
    require "efmls-configs.formatters.gofumpt",
    require "efmls-configs.formatters.goimports",
    require "efmls-configs.formatters.golines",
    require "efmls-configs.linters.go_revive",
    require "efmls-configs.linters.staticcheck",
  },
  json = {
    require "efmls-configs.formatters.fixjson",
    require "efmls-configs.formatters.jq",
    require "efmls-configs.formatters.prettier_d",
    require "efmls-configs.linters.jq",
  },
  lua = {
    require "efmls-configs.formatters.stylua",
    -- require "efmls-configs.linters.luacheck", - it's annoying
    -- require "efmls-configs.linters.selene", - it's annoying
  },
  markdown = {
    require "efmls-configs.formatters.cbfmt",
    require "efmls-configs.formatters.mdformat",
    require "efmls-configs.linters.markdownlint",
  },
  misc = {
    require "efmls-configs.linters.codespell",
    require "efmls-configs.linters.cspell",
    require "efmls-configs.linters.redpen",
    require "efmls-configs.linters.vale",
    require "efmls-configs.linters.write_good",
  },
  python = {
    require "efmls-configs.formatters.autopep8",
    require "efmls-configs.formatters.black",
    require "efmls-configs.formatters.isort",
    require "efmls-configs.formatters.ruff",
    -- require "efmls-configs.linters.flake8", - doean't autofix
    -- require "efmls-configs.linters.mypy",
    -- require "efmls-configs.linters.pylint", - it's not useful usually
    -- require "efmls-configs.linters.ruff",
    require "efmls-configs.linters.vulture",
  },
  rust = {
    require "efmls-configs.formatters.rustfmt",
  },
  sh = {
    require "efmls-configs.formatters.beautysh",
    require "efmls-configs.linters.bashate",
    require "efmls-configs.linters.shellcheck",
  },
  sql = {
    require "efmls-configs.formatters.sql-formatter",
    require "efmls-configs.linters.sqlfluff",
  },
  tex = {
    require "efmls-configs.formatters.latexindent",
    require "efmls-configs.linters.chktex",
  },
  toml = {
    require "efmls-configs.formatters.taplo",
  },
  typescript = {
    require "efmls-configs.formatters.eslint",
    require "efmls-configs.formatters.prettier_d",
    require "efmls-configs.linters.eslint_d",
  },
  yaml = {
    require "efmls-configs.formatters.prettier_d",
    require "efmls-configs.formatters.yq",
    require "efmls-configs.linters.actionlint",
    require "efmls-configs.linters.ansible_lint",
    require "efmls-configs.linters.yamllint",
  },
  zsh = {
    require "efmls-configs.formatters.beautysh",
    require "efmls-configs.linters.bashate",
    require "efmls-configs.linters.shellcheck",
  },
}

-- Or use the defaults provided by this plugin
-- check doc/SUPPORTED_LIST.md for the supported languages
--
-- local languages = require('efmls-configs.defaults').languages()

local opts = {
  filetypes = vim.tbl_keys(languages),
  settings = {
    rootMarkers = { ".git/" },
    languages = languages,
  },
  init_options = {
    documentFormatting = true,
    documentRangeFormatting = true,
  },
  on_attach = function(client, bufnr)
    --   if client.supports_method("textDocument/formatting") then
    --     vim.api.nvim_clear_autocmds({
    --       group = augroup,
    --       buffer = bufnr,
    --     })
    --     vim.api.nvim_create_autocmd("BufWritePre", {
    --       group = augroup,
    --       buffer = bufnr,
    --       callback = function()
    --         vim.lsp.buf.format({ bufnr = bufnr })
    --       end,
    --     })
    --   end
    --   OR EFM way
    -- local lsp_fmt_group = vim.api.nvim_create_augroup("LspFormattingGroup", {})
    -- vim.api.nvim_create_autocmd("BufWritePost", {
    --   group = lsp_fmt_group,
    --   callback = function(ev)
    --     local efm = vim.lsp.get_active_clients { name = "efm", bufnr = ev.buf }
    --
    --     if vim.tbl_isempty(efm) then
    --       return
    --     end
    --
    --     vim.lsp.buf.format { name = "efm" }
    --   end,
    -- })
  end,
}

return opts
