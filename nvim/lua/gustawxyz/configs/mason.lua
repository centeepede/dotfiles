local opts = {
  ensure_installed = {
    -- efm (linters and formatters)
    "efm",

    -- cmake
    "gersemi",
    "cmakelint",
    -- cpp
    "clangd",
    "clang-format",
    "codelldb",
    -- css
    "prettierd",
    "stylelint",
    -- docker
    "hadolint",
    -- git
    "gitlint",
    --Go
    "gofumpt",
    "goimports",
    "golines",
    "gopls",
    "revive",
    -- json
    "fixjson",
    "jq",
    --lua
    "lua-language-server",
    "stylua",
    -- "luacheck", - disabled in efm
    -- "selene", - disabled in efm
    -- markdown
    "cbfmt",
    "dprint",
    "mdformat",
    "markdownlint",
    -- spelling
    "codespell",
    "cspell",
    "vale",
    "write-good",
    -- "typos", - unsupported by efm
    -- "misspell", - unsupported by efm
    --python
    "debugpy",
    "pyright",

    "autopep8",
    "black",
    "isort",
    "flake8",
    "mypy",
    "pylint",
    "ruff",
    --Rust
    "rust-analyzer",
    "rustfmt",
    -- sh
    "bash-language-server",

    "beautysh",
    "shellcheck",
    -- sql
    "sqlfmt",
    "sqlfluff",
    -- tex
    -- "latexindent" - missing in Mason
    -- "chktex" - missing in Mason
    -- toml
    "taplo",
    -- typescript/javascript
    "deno",
    "eslint-lsp",
    "eslint_d",
    "js-debug-adapter",
    "prettierd",
    "prettier",
    "typescript-language-server",
    -- yaml
    "yq",
    "actionlint",
    "ansible-lint",
    "yamllint",

  },
  PATH = "skip",

  ui = {
    icons = {
      package_pending = " ",
      package_installed = "󰄳 ",
      package_uninstalled = " 󰚌",
    },

    keymaps = {
      toggle_server_expand = "<CR>",
      install_server = "i",
      update_server = "u",
      check_server_version = "c",
      update_all_servers = "U",
      check_outdated_servers = "C",
      uninstall_server = "X",
      cancel_installation = "<C-c>",
    },
  },

  max_concurrent_installers = 10,
}

require("mason").setup(opts)

vim.api.nvim_create_user_command("MasonInstallAll", function()
  vim.cmd("MasonInstall " .. table.concat(opts.ensure_installed, " "))
end, {})

vim.g.mason_binaries_list = opts.ensure_installed
