local M = {}
-- M.capabilities = vim.lsp.protocol.make_client_capabilities()
M.capabilities = require("cmp_nvim_lsp").default_capabilities()

M.capabilities.textDocument.completion.completionItem = {
  documentationFormat = { "markdown", "plaintext" },
  snippetSupport = true,
  preselectSupport = true,
  insertReplaceSupport = true,
  labelDetailsSupport = true,
  deprecatedSupport = true,
  commitCharactersSupport = true,
  tagSupport = { valueSet = { 1 } },
  resolveSupport = {
    properties = {
      "documentation",
      "detail",
      "additionalTextEdits",
    },
  },
}

local lspconfig = require "lspconfig"
local util = require "lspconfig/util"

lspconfig.awk_ls.setup { capabilities = M.capabilities }
lspconfig.awk_ls.setup { capabilities = M.capabilities }
lspconfig.bashls.setup { capabilities = M.capabilities }
lspconfig.cmake.setup { capabilities = M.capabilities }
lspconfig.cssls.setup { capabilities = M.capabilities }
lspconfig.docker_compose_language_service.setup { capabilities = M.capabilities }
lspconfig.dockerls.setup { capabilities = M.capabilities }
lspconfig.gdscript.setup { capabilities = M.capabilities }
lspconfig.gdshader_lsp.setup { capabilities = M.capabilities }
lspconfig.glsl_analyzer.setup { capabilities = M.capabilities }
lspconfig.html.setup { capabilities = M.capabilities }
lspconfig.htmx.setup { capabilities = M.capabilities }
lspconfig.jqls.setup { capabilities = M.capabilities }
lspconfig.jsonls.setup { capabilities = M.capabilities }
lspconfig.nginx_language_server.setup { capabilities = M.capabilities }
lspconfig.rnix.setup { capabilities = M.capabilities }
lspconfig.postgres_lsp.setup { capabilities = M.capabilities }
lspconfig.rust_analyzer.setup { capabilities = M.capabilities }
lspconfig.sqlls.setup { capabilities = M.capabilities }

-- spelling
lspconfig.typos_lsp.setup { capabilities = M.capabilities }
lspconfig.textlsp.setup {
  capabilities = M.capabilities,
  filetypes = { "text", "tex", "org", "markdown", "norg", "tex" },
}

lspconfig.dprint.setup {
  capabilities = M.capabilities,
  root_dir = util.root_pattern(".git")
}
lspconfig.marksman.setup {
  capabilities = M.capabilities,
  filetypes = { "markdown", "markdown.mdx", "txt" },
}
lspconfig.ltex.setup {
  capabilities = M.capabilities,
  settings = {
    ltex = {
      language = "en-GB",
    },
  },
}
lspconfig.lua_ls.setup {
  capabilities = M.capabilities,

  settings = {
    Lua = {
      diagnostics = {
        globals = { "vim" },
      },
      workspace = {
        library = {
          [vim.fn.expand "$VIMRUNTIME/lua"] = true,
          [vim.fn.expand "$VIMRUNTIME/lua/vim/lsp"] = true,
          [vim.fn.stdpath "data" .. "/lazy/ui/nvchad_types"] = true,
          [vim.fn.stdpath "data" .. "/lazy/lazy.nvim/lua/lazy"] = true,
        },
        maxPreload = 100000,
        preloadFileSize = 10000,
      },
    },
  },
}
lspconfig.clangd.setup {
  on_attach = function(client, bufnr)
    client.server_capabilities.signatureHelpProvider = false
  end,
  capabilities = M.capabilities,
}
lspconfig.pyright.setup {
  capabilities = M.capabilities,
  filetypes = { "python" },
  root_dir = util.root_pattern("Config", "pyrightconfig.json", ".git"),
}
lspconfig.gopls.setup {
  capabilities = M.capabilities,
  cmd = { "gopls" },
  filetypes = { "go", "gomod", "gowork", "gotmpl" },
  root_dir = util.root_pattern("go.work", "go.mod", ".git"),
  settings = {
    gopls = {
      completeUnimported = true,
      usePlaceholders = true,
      analyses = {
        unusedparams = true,
      },
    },
  },
}

-- TypeScript/JavaScript

lspconfig.tsserver.setup {
  capabilities = M.capabilities,
  init_options = {
    preferences = {
      disableSuggestions = true,
    },
  },
  commands = {
    OrganizeImports = {
      function()
        local params = {
          command = "_typescript.organizeImports",
          arguments = { vim.api.nvim_buf_get_name(0) },
        }
        vim.lsp.buf.execute_command(params)
      end,
      description = "Organize Imports",
    },
  },
}

lspconfig.eslint.setup {
  capabilities = M.capabilities,
  settings = {
    nodePath = os.getenv "HOME" .. "/.local/share/nvim/mason/packages/eslint-lsp/node_modules",
  },
}

return M
