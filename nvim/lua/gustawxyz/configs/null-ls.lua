local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
local null_ls = require "null-ls"
local cspell = require "cspell"

local opts = {
  sources = {
    -- cpp
    null_ls.builtins.formatting.clang_format,
    null_ls.builtins.diagnostics.cmake_lint,
    null_ls.builtins.diagnostics.cppcheck,
    -- python
    null_ls.builtins.formatting.black,
    -- lua
    null_ls.builtins.formatting.stylua,
    -- go
    null_ls.builtins.formatting.gofumpt,
    null_ls.builtins.formatting.goimports_reviser,
    null_ls.builtins.formatting.golines,
    -- sehll scripts
    null_ls.builtins.diagnostics.zsh,
    null_ls.builtins.diagnostics.shellcheck,
    null_ls.builtins.hover.printenv,
    -- json
    null_ls.builtins.formatting.fixjson,
    -- tags
    null_ls.builtins.completion.tags,
    -- javaScript/TypeScript
    null_ls.builtins.diagnostics.eslint,
    null_ls.builtins.formatting.prettier,
    -- shell
    null_ls.builtins.formatting.shellharden,
    null_ls.builtins.formatting.shfmt,
    null_ls.builtins.diagnostics.dotenv_linter,
    null_ls.builtins.hover.printenv,
    null_ls.builtins.diagnostics.zsh,
    -- git
    null_ls.builtins.diagnostics.commitlint,
    -- spelling
    null_ls.builtins.completion.spell,
    null_ls.builtins.diagnostics.codespell,
    null_ls.builtins.formatting.codespell,
    -- general formatting
    null_ls.builtins.diagnostics.todo_comments,
    null_ls.builtins.diagnostics.trail_space,
  },
  on_attach = function(client, bufnr)
    --   if client.supports_method("textDocument/formatting") then
    --     vim.api.nvim_clear_autocmds({
    --       group = augroup,
    --       buffer = bufnr,
    --     })
    --     vim.api.nvim_create_autocmd("BufWritePre", {
    --       group = augroup,
    --       buffer = bufnr,
    --       callback = function()
    --         vim.lsp.buf.format({ bufnr = bufnr })
    --       end,
    --     })
    --   end
  end,
}
return opts
