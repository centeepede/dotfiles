require("auto-session").setup {
  log_level = "error",
  -- auto_session_enable_last_session = true,
  auto_session_enabled = true,
  auto_session_create_enabled = true,
  auto_save_enabled = true,
  auto_restore_enabled = true,
  auto_session_root_dir = vim.fn.stdpath('data').."/sessions/",
  auto_session_suppress_dirs = { "~/", "~/Projects", "~/Documents", "~/Downloads", "/" },
  -- ⚠️ This will only work if Telescope.nvim is installed
  -- The following are already the default values, no need to provide them if these are already the settings you want.
  session_lens = {
    -- If load_on_setup is set to false, one needs to eventually call `require("auto-session").setup_session_lens()` if they want to use session-lens.
    load_on_setup = true,
    theme_conf = { border = true },
    previewer = false,
  },
}
