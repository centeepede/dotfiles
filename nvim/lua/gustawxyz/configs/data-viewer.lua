local config = {
  autoDisplayWhenOpenFile = true,
  maxLineEachTable = 100,
  columnColorEnable = true,
  -- columnColorRoulette = { -- Highlight groups
  --   "DataViewerColumn0",
  --   "DataViewerColumn1",
  --   "DataViewerColumn2",
  -- },
  view = {
    float = true, -- False will open in current window
    width = 0.9, -- Less than 1 means ratio to screen width, valid when float = true
    height = 0.9, -- Less than 1 means ratio to screen height, valid when float = true
    zindex = 20, -- Valid when float = true
  },
  keymap = {
    quit = "q",
    next_table = "<C-l>",
    prev_table = "<C-h>",
  },
}

return config
