-- system clipboard (picked up by osc52 to use SSH)
vim.keymap.set({ "n", "x" }, "<leader>y", '"+y', { desc = "Yank to system clipboard" })
vim.keymap.set("n", "<leader>Y", '"+Y', { desc = "Yank to system clipboard" })
vim.keymap.set({ "n", "x" }, "<leader>p", '"+p', { desc = "Paste from system clipboard" })
vim.keymap.set("n", "<leader>P", '"+P', { desc = "Paste from system clipboard" })

vim.keymap.set("i", "<C-b>", "<ESC>^i", { desc = "Beginning of line" })
vim.keymap.set("i", "<C-e>", "<End>", { desc = "End of line" })

-- navigate within insert mode
vim.keymap.set("i", "<C-h>", "<Left>", { desc = "Move left" })
vim.keymap.set("i", "<C-l>", "<Right>", { desc = "Move right" })
vim.keymap.set("i", "<C-j>", "<Down>", { desc = "Move down" })
vim.keymap.set("i", "<C-k>", "<Up>", { desc = "Move up" })

vim.keymap.set("n", "<C-t>n", "<Cmd>tabnew<CR>", { desc = "Create new tab" })
vim.keymap.set("n", "<C-t>q", "<Cmd>tabclose<CR>", { desc = "Close current tab" })

vim.keymap.set("n", "<Esc>", ":noh <CR>", { desc = "Clear highlights" })
-- switch between windows
-- vim.keymap.set("n", "<C-h>", "<C-w>h", { desc = "Window left" })
-- vim.keymap.set("n", "<C-l>", "<C-w>l", { desc = "Window right" })
-- vim.keymap.set("n", "<C-j>", "<C-w>j", { desc = "Window down" })
-- vim.keymap.set("n", "<C-k>", "<C-w>k", { desc = "Window up" })
vim.keymap.set({ "n", "t" }, "<C-h>", "<cmd> TmuxNavigateLeft<CR>", { desc = "window left" })
vim.keymap.set({ "n", "t" }, "<C-l>", "<cmd> TmuxNavigateRight<CR>", { desc = "window right" })
vim.keymap.set({ "n", "t" }, "<C-j>", "<cmd> TmuxNavigateDown<CR>", { desc = "window down" })
vim.keymap.set({ "n", "t" }, "<C-k>", "<cmd> TmuxNavigateUp<CR>", { desc = "window up" })

-- terminal
-- vim.keymap.set("t", "<C-x>", vim.api.nvim_replace_termcodes("<C-\\><C-N>", true, true, true), {desc = "Escape terminal mode"} )

vim.keymap.set("t", "jkj", "<C-\\><C-n>", { buffer = 0, desc = "exit term on up-down-up" })
vim.keymap.set("t", "kjk", "<C-\\><C-n>", { buffer = 0, desc = "exit term on up-down-up" })

-- save
vim.keymap.set("n", "<leader>w", ':call mkdir(expand("%:p:h"), "p")<CR>:w<CR>', { desc = "Save file" })
vim.api.nvim_create_user_command('W',
  function(opts)
    vim.cmd.call('mkdir(expand("%:p:h"), "p")')
    vim.cmd.w()
  end,
  { desc = "Save file"  })

-- chmod
vim.keymap.set("n", "<leader>ex", "<Cmd>!chmod +x %<CR>", { desc = "chmod +x" })
vim.keymap.set("n", "<leader>eX", "<Cmd>!chmod -x %<CR>", { desc = "chmod -x" })

-- Copy all
vim.keymap.set("n", "<C-c>", "<cmd> %y+ <CR>", { desc = "Copy whole file" })

-- Allow moving the cursor through wrapped lines with j, k, <Up> and <Down>
-- http://www.reddit.com/r/vim/comments/2k4cbr/problem_with_gj_and_gk/
-- empty mode is same as using <cmd> :map
-- also don't use gj|k when in operator pending mode, so it doesn't alter d, y or c behaviour
-- vim.keymap.set("n", "j", 'v:count || mode(1)0:1 == "no" ? "j" : "gj"', { desc = "Move down", expr = true })
-- vim.keymap.set("n", "k", 'v:count || mode(1)0:1 == "no" ? "k" : "gk"', { desc = "Move up", expr = true })
-- vim.keymap.set("n", "<Up>", 'v:count || mode(1)0:1 == "no" ? "k" : "gk"', { desc = "Move up", expr = true })
-- vim.keymap.set("n", "<Down>", 'v:count || mode(1)0:1 == "no" ? "j" : "gj"', { desc = "Move down", expr = true })

-- new buffer
vim.keymap.set("n", "<leader>b", "<cmd> enew <CR>", { desc = "New buffer" })

vim.keymap.set("n", "<leader>fm", function()
  vim.lsp.buf.format { async = true }
end, { desc = "LSP formatting" })

-- vim.keymap.set("v", "<Up>", 'v:count || mode(1)[0:1] == "no" ? "k" : "gk"', { desc = "Move up", expr = true })
-- vim.keymap.set("v", "<Down>", 'v:count || mode(1)[0:1] == "no" ? "j" : "gj"', { desc = "Move down", expr = true })
--
-- vim.keymap.set("x", "j", 'v:count || mode(1)[0:1] == "no" ? "j" : "gj"', { desc = "Move down", expr = true })
-- vim.keymap.set("x", "k", 'v:count || mode(1)[0:1] == "no" ? "k" : "gk"', { desc = "Move up", expr = true })
-- Don't copy the replaced text after pasting in visual mode
-- https://vim.fandom.com/wiki/Replace_a_word_with_yanked_text#Alternative_mapping_for_paste
vim.keymap.set("x", "p", 'p:let @+=@0<CR>:let @"=@0<CR>', { desc = "Dont copy replaced text", silent = true })

-- lspconfig
vim.keymap.set("n", "<leader>cd", function()
  vim.diagnostic.open_float()
end, { desc = "Show line diagnostics in a popup" })

vim.keymap.set("n", "gh", "<cmd> ClangdSwitchSourceHeader <CR>", { desc = "clangd switch source/header" })

vim.keymap.set("n", "gD", function()
  vim.lsp.buf.declaration()
end, { desc = "LSP declaration" })

vim.keymap.set("n", "gd", function()
  vim.lsp.buf.definition()
end, { desc = "LSP definition" })

vim.keymap.set("n", "K", function()
  vim.lsp.buf.hover()
end, { desc = "LSP hover" })

vim.keymap.set("n", "gi", function()
  vim.lsp.buf.implementation()
end, { desc = "LSP implementation" })

vim.keymap.set("n", "<leader>ls", function()
  vim.lsp.buf.signature_help()
end, { desc = "LSP signature help" })

vim.keymap.set("n", "<leader>D", function()
  vim.lsp.buf.type_definition()
end, { desc = "LSP definition type" })

vim.keymap.set("n", "<leader>ra", function()
  vim.lsp.buf.rename()
end, { desc = "LSP rename" })

vim.keymap.set("n", "<leader>ca", function()
  vim.lsp.buf.code_action()
end, { desc = "LSP code action" })

vim.keymap.set("n", "gr", function()
  vim.lsp.buf.references()
end, { desc = "LSP references" })

vim.keymap.set("n", "<leader>f", function()
  vim.diagnostic.open_float { border = "rounded" }
end, { desc = "Floating diagnostic" })

vim.keymap.set("n", "[d", function()
  vim.diagnostic.goto_prev { float = { border = "rounded" } }
end, { desc = "Goto prev" })

vim.keymap.set("n", "]d", function()
  vim.diagnostic.goto_next { float = { border = "rounded" } }
end, { desc = "Goto next" })

vim.keymap.set("n", "<leader>q", function()
  vim.diagnostic.setloclist()
end, { desc = "Diagnostic setloclist" })

vim.keymap.set("n", "<leader>wa", function()
  vim.lsp.buf.add_workspace_folder()
end, { desc = "Add workspace folder" })

vim.keymap.set("n", "<leader>wr", function()
  vim.lsp.buf.remove_workspace_folder()
end, { desc = "Remove workspace folder" })

vim.keymap.set("n", "<leader>wl", function()
  print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end, { desc = "List workspace folders" })

-- barbar (tabline)
-- vim.keymap.set("n", "<tab>", "<Cmd>BufferNext<CR>", { desc = "Goto next buffer" })
-- vim.keymap.set("n", "<S-tab>", "<Cmd>BufferPrevious<CR>", { desc = "Goto prev buffer" })
-- vim.keymap.set("n", "<leader>x", "<Cmd>BufferClose<CR>", { desc = "Close buffer" })
