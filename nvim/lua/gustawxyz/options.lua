local opt = vim.opt
local g = vim.g

vim.opt.spelllang = 'en_us,pl'
vim.opt.spell = true

opt.colorcolumn = "80"

opt.undodir = os.getenv("HOME") .. "/.local/state/nvim/undo"

opt.laststatus = 3 -- global statusline
opt.showmode = false

opt.backup = false
opt.swapfile = false

-- opt.clipboard = "unnamedplus"
opt.cursorline = true

-- Indenting
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.shiftround = false
opt.expandtab = true
opt.autoindent = true
opt.smartindent = true

opt.fillchars = { eob = " " }
opt.smartcase = true
opt.mouse = "a"

-- Numbers
opt.number = true
opt.numberwidth = 2
opt.ruler = false

-- disable nvim intro
opt.shortmess:append "sI"

opt.signcolumn = "yes"
opt.splitbelow = true
opt.splitright = true
opt.termguicolors = true
opt.timeoutlen = 400
opt.undofile = true

-- make sure there is always a gap between the cursor and bottom of the screen
opt.scrolloff = 8

-- interval for writing swap file to disk, also used by gitsigns
opt.updatetime = 250

-- go to previous/next line with h,l,left arrow and right arrow
-- when cursor reaches end/beginning of line
-- opt.whichwrap:append "<>[]hl"
g.dap_virtual_text = true


-------------------------------------- autocmds ------------------------------------------
local autocmd = vim.api.nvim_create_autocmd

-- overrule ftplugin
autocmd("FileType", {
  pattern = "*",
  callback = function()
    opt.formatoptions:remove({"o"})
  end,
})

-- dont list quickfix buffers
-- autocmd("FileType", {
--   pattern = "qf",
--   callback = function()
--     vim.opt_local.buflisted = false
--   end,
-- })
-- replace by after/ftplugin/qf.lua - not sure if that will work though

-- disable some default providers
for _, provider in ipairs { "node", "perl", "python3", "ruby" } do
  vim.g["loaded_" .. provider .. "_provider"] = 0
end

-- add binaries installed by mason.nvim to path
local is_windows = vim.loop.os_uname().sysname == "Windows_NT"
vim.env.PATH = vim.fn.stdpath "data" .. "/mason/bin" .. (is_windows and ";" or ":") .. vim.env.PATH
