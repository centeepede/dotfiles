local dap_python_arguments_cache = ""
local plugins = {
  {
    "catppuccin/nvim",
    lazy = false,
    priority = 1000,
    -- config = function()
    --   vim.cmd.colorscheme "catppuccin-mocha"
    -- end,
  },
  {
    "bluz71/vim-moonfly-colors",
    name = "moonfly",
    lazy = false,
    priority = 1000,
    config = function()
      vim.cmd.colorscheme "moonfly"
    end,
  },
  {
    "nvim-tree/nvim-web-devicons",
    config = function()
      require("nvim-web-devicons").setup()
    end,
  },
  {
    "christoomey/vim-tmux-navigator",
    lazy = false,
  },
  {
    "nvim-lualine/lualine.nvim",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    event = "VeryLazy",
    config = function()
      require("lualine").setup()
    end,
  },
  {
    "ojroques/nvim-osc52",
    event = "VeryLazy",
    config = function()
      require("osc52").setup()
    end,
    init = function()
      local function copy(lines, _)
        require("osc52").copy(table.concat(lines, "\n"))
      end
      local function paste()
        return { vim.fn.split(vim.fn.getreg "", "\n"), vim.fn.getregtype "" }
      end
      if os.getenv "SSH_TTY" and not (os.getenv "DISPLAY" or os.getenv "TMUX") then
        vim.g.clipboard = {
          name = "osc52",
          copy = { ["+"] = copy, ["*"] = copy },
          paste = { ["+"] = paste, ["*"] = paste },
        }
      end
    end,
  },
  {
    "karb94/neoscroll.nvim",
    event = "VeryLazy",
    config = function()
      require("neoscroll").setup()
    end,
  },
  {
    "m00qek/baleia.nvim",
    event = "VeryLazy",
    ft = { "bash", "zsh" },
    config = function()
      require("baleia").setup()
    end,
    keys = {
      {
        "<leader>ctA",
        "<cmd> BaleiaColorize<CR>",
        mode = "n",
        desc = "Toggle ANSI color highlighting",
      },
    },
  },
  {
    "vidocqh/data-viewer.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "kkharji/sqlite.lua", -- Optional, sqlite support
    },
    opts = function()
      return require "gustawxyz.configs.data-viewer"
    end,
    config = function(_, opts)
      require("data-viewer").setup(opts)
    end,
  },
  {
    "uga-rosa/ccc.nvim",
    event = "VeryLazy",
    keys = {
      {
        "<leader>cp",
        "<cmd> CccPick<CR>",
        mode = "n",
        desc = "Pick Color",
      },
      {
        "<leader>ct",
        "<cmd> CccHighlighterToggle<CR>",
        mode = "n",
        desc = "Toggle color highlighting",
      },
    },
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    event = "VeryLazy",
    opts = function()
      return require "gustawxyz.configs.blankline"
    end,
    keys = {},
    config = function(_, opts)
      require("ibl").setup()
    end,
  },
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    keys = {
      {
        "<leader>tt",
        "<cmd> ToggleTerm<CR>",
        mode = "n",
        desc = "Open terminal",
      },
      {
        "<C-x>",
        function()
          vim.cmd "ToggleTerm"
        end,
        mode = "t",
        desc = "Escape terminal mode",
      },
    },
    config = function()
      require "gustawxyz.configs.toggleterm"
    end,
  },
  {
    "michaelb/sniprun",
    dependencies = {
      "akinsho/toggleterm.nvim",
    },
    cmd = { "SnipRun", "SnipInfo", "SnipReset", "SnipReplMemoryClean", "SnipClose", "SnipLive" },
    keys = {
      { "<leader>ce", "ggVG<Plug>SnipRun", mode = "n", desc = "Execute code" },
      {
        "<leader>ce",
        function()
          require("sniprun").run "v"
        end,
        mode = "x",
        desc = "Execute code",
      },
      {
        "<leader>cel",
        function()
          require("sniprun.live_mode").toggle()
        end,
        mode = "n",
        desc = "Toggle live mode",
      },
    },
    build = "cargo build --release",
    config = function()
      require "gustawxyz.configs.sniprun"
    end,
  },
  -- {
  --   "ggandor/leap.nvim",
  --   config = function()
  --     require("leap").add_default_mappings()
  --   end,
  -- },
  {
    "ThePrimeagen/harpoon",
    keys = {
      {
        "<leader>hc",
        function()
          require("harpoon.mark").add_file()
        end,
        mode = "n",
        desc = "Harpoon add(create) file",
      },
      {
        "<leader>hv",
        function()
          require("harpoon.ui").toggle_quick_menu()
        end,
        mode = "n",
        desc = "Harpoon quick view",
      },
      {
        "<leader>ha",
        function()
          require("harpoon.ui").nav_file(4)
        end,
        mode = "n",
        desc = "F 4",
      },
      {
        "<leader>hs",
        function()
          require("harpoon.ui").nav_file(3)
        end,
        mode = "n",
        desc = "F 3",
      },
      {
        "<leader>hd",
        function()
          require("harpoon.ui").nav_file(2)
        end,
        mode = "n",
        desc = "F 2",
      },
      {
        "<leader>hf",
        function()
          require("harpoon.ui").nav_file(1)
        end,
        mode = "n",
        desc = "F 1",
      },
    },
    config = function()
      require("telescope").load_extension "harpoon"
      require("harpoon").setup()
    end,
  },
  {
    "rmagatti/auto-session",
    lazy = false,
    keys = {
      {
        "<leader>fp",
        function()
          local auto_session = require "auto-session.session-lens"
          auto_session.search_session()
        end,
        mode = "n",
        desc = "Open Sessions' picker",
      },
    },
    config = function()
      require "gustawxyz.configs.auto-session"
    end,
  },
  {
    "mbbill/undotree",
    lazy = "VeryLazy",
    keys = {
      { "<leader>u", "<cmd> UndotreeToggle<CR>", mode = "n", desc = "Open UndeoTree" },
    },
  },
  {
    "stevearc/oil.nvim",
    lazy = false,
    cmd = { "Oil" },
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    config = function()
      require "gustawxyz.configs.oil"
    end,
    keys = {
      {
        "<C-n>",
        function()
          vim.cmd ":Oil"
        end,
        mode = "n",
        desc = "Toggle Oil",
      },
      {
        "<leader>gf",
        function()
          local current_file_dir = vim.fs.dirname(vim.api.nvim_buf_get_name(0))
          local git_dir = vim.fs.find({ ".git" }, { upward = true, type = "directory", path = current_file_dir })[1]
          local repo_dir = vim.fs.dirname(git_dir)
          local arg = ""
          if repo_dir ~= "" and repo_dir ~= nil then
            vim.cmd(":lcd " .. repo_dir)
            arg = " " .. repo_dir
          elseif not current_file_dir:match(vim.fn.getcwd()) then
            arg = current_file_dir
          end
          vim.cmd(":Oil" .. arg)
        end,
        mode = "n",
        desc = "Oil in git root",
      },
    },
  },
  {
    "anuvyklack/pretty-fold.nvim",
    lazy = false,
    config = function()
      require("pretty-fold").setup()
    end,
  },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    config = function()
      require("nvim-surround").setup {
        -- Configuration here, or leave empty to use defaults
      }
    end,
  },
  {
    "chentoast/marks.nvim",
    event = "VeryLazy",
    keys = {
      { "<leader>md", "<cmd> delm A-Za-z0-9[]<>.<CR>", mode = "n", desc = "Delete all marks" },
    },
    config = function()
      require("marks").setup()
    end,
  },
  -- {
  --   "NvChad/nvterm",
  --   init = function()
  --   end,
  --   config = function(_, opts)
  --     require("nvterm").setup(opts)
  --   end,
  -- },
  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        -- defaults
        "vim",
        "lua",

        --rest
        "norg",
        "bash",
        "cmake",
        "yaml",
        "json",
        "python",
        "c",
        "cpp",
        "rust",
      },
    },
    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter-textobjects",
    event = "VeryLazy",
    keys = {
      { "ig", ":<C-U>Gitsigns select_hunk<CR>", mode = { "o", "x" }, desc = "Inside git hunk" },
    },
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
    },
    config = function()
      require "gustawxyz.configs.treesitter-textobjects"
    end,
  },
  {
    "sindrets/diffview.nvim",
    event = "VeryLazy",
  },
  {
    "nvim-lua/plenary.nvim",
    --event = "VeryLazy",
  },
  {
    "lewis6991/gitsigns.nvim",
    ft = { "gitcommit", "diff" },
    keys = {
      {
        "]c",
        function()
          if vim.wo.diff then
            return "]c"
          end
          vim.schedule(function()
            require("gitsigns").next_hunk()
          end)
          return "<Ignore>"
        end,
        mode = "n",
        desc = "Jump to next hunk",
        expr = true,
      },

      {
        "[c",
        function()
          if vim.wo.diff then
            return "[c"
          end
          vim.schedule(function()
            require("gitsigns").prev_hunk()
          end)
          return "<Ignore>"
        end,
        mode = "n",
        desc = "Jump to prev hunk",
        expr = true,
      },

      {
        "<leader>gr",
        function()
          require("gitsigns").reset_hunk()
        end,
        mode = "n",
        desc = "Reset hunk",
      },

      {
        "<leader>gr",
        function()
          require("gitsigns").reset_hunk { vim.fn.line ".", vim.fn.line "v" }
        end,
        mode = "x",
        desc = "Reset hunk",
      },

      {
        "<leader>gp",
        function()
          require("gitsigns").preview_hunk()
        end,
        mode = "n",
        desc = "Preview hunk",
      },

      {
        "<leader>gb",
        function()
          package.loaded.gitsigns.blame_line {}
          -- require("gitsigns").toggle_current_line_blame()
        end,
        mode = "n",
        desc = "Blame line",
      },

      {
        "<leader>gs",
        function()
          require("gitsigns").stage_hunk()
        end,
        mode = "n",
        desc = "Stage hunk",
      },

      {
        "<leader>gs",
        function()
          require("gitsigns").stage_hunk { vim.fn.line ".", vim.fn.line "v" }
        end,
        mode = "x",
        desc = "Stage hunk",
      },

      {
        "<leader>gus",
        function()
          require("gitsigns").undo_stage_hunk()
        end,
        mode = "n",
        desc = "Undo stage hunk",
      },

      {
        "<leader>gS",
        function()
          require("gitsigns").stage_buffer()
        end,
        mode = "n",
        desc = "Stage buffer",
      },

      {
        "<leader>gdd",
        function()
          require("gitsigns").toggle_deleted()
        end,
        mode = "n",
        desc = "Toggle deleted",
      },

      {
        "<leader>gdc",
        function()
          require("gitsigns").diffthis "~"
        end,
        mode = "n",
        desc = "Diff this file last commit",
      },

      {
        "<leader>gdu",
        function()
          require("gitsigns").diffthis()
        end,
        mode = "n",
        desc = "Diff this file uncommited",
      },
    },
    init = function()
      -- load gitsigns only when a git file is opened
      vim.api.nvim_create_autocmd({ "BufRead" }, {
        group = vim.api.nvim_create_augroup("GitSignsLazyLoad", { clear = true }),
        callback = function()
          vim.fn.system("git -C " .. '"' .. vim.fn.expand "%:p:h" .. '"' .. " rev-parse")
          if vim.v.shell_error == 0 then
            vim.api.nvim_del_augroup_by_name "GitSignsLazyLoad"
            vim.schedule(function()
              require("lazy").load { plugins = { "gitsigns.nvim" } }
            end)
          end
        end,
      })
    end,
    opts = function()
      return require("gustawxyz.configs.gitsigns").gitsigns
    end,
    config = function(_, opts)
      require("gitsigns").setup(opts)
    end,
  },
  {
    "NeogitOrg/neogit",
    event = "VeryLazy",
    keys = {
      {
        "<leader>gg",
        function()
          local neovim = require "neogit"
          local git_dir = vim.fs.find(
            { ".git" },
            { upward = true, type = "directory", path = vim.fs.dirname(vim.api.nvim_buf_get_name(0)) }
          )[1]
          local repo_dir = vim.fs.dirname(git_dir)
          if repo_dir ~= "" and repo_dir ~= nil then
            neovim.open { cwd = vim.fs.dirname(git_dir) }
          else
            neovim.open()
          end
        end,
        mode = "n",
        desc = "show neogit",
      },
    },

    dependencies = {
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "neovim/nvim-lspconfig",
      "nvim-telescope/telescope.nvim",
    },
    config = function()
      require "gustawxyz.configs.neogit"
      require("neogit").setup()
    end,
  },
  {
    "toppair/peek.nvim",
    build = "deno task --quiet build:fast",
    keys = {
      {
        "<leader>op",
        function()
          local peek = require "peek"
          if peek.is_open() then
            peek.close()
          else
            peek.open()
          end
        end,
        desc = "Peek (Markdown Preview)",
      },
    },
    opts = { theme = "dark", app = "browser" },
  },
  {
    "nvim-telescope/telescope.nvim",
    dependencies = "nvim-treesitter/nvim-treesitter",
    cmd = "Telescope",
    keys = {

      { "<leader>ff", "<cmd> Telescope find_files <CR>", mode = "n", desc = "Find files" },
      {
        "<leader>fa",
        "<cmd> Telescope find_files follow=true no_ignore=true hidden=true <CR>",
        mode = "n",
        desc = "Find all",
      },
      { "<leader>fw", "<cmd> Telescope live_grep <CR>", mode = "n", desc = "Live grep" },
      { "<leader>fb", "<cmd> Telescope buffers <CR>", mode = "n", desc = "Find buffers" },
      { "<leader>fh", "<cmd> Telescope help_tags <CR>", mode = "n", desc = "Help page" },
      { "<leader>fo", "<cmd> Telescope oldfiles <CR>", mode = "n", desc = "Find oldfiles" },
      { "<leader>fz", "<cmd> Telescope current_buffer_fuzzy_find <CR>", mode = "n", desc = "Find in current buffer" },
      -- git
      { "<leader>gc", "<cmd> Telescope git_commits <CR>", mode = "n", desc = "Git commits" },
      { "<leader>gt", "<cmd> Telescope git_status <CR>", mode = "n", desc = "Git status" },
      { "<leader>ma", "<cmd> Telescope marks <CR>", mode = "n", desc = "telescope bookmarks" },
      { "<leader>hk", "<cmd> Telescope keymaps <CR>", mode = "n", desc = "Git status" },
      {
        "<leader>cr",
        function()
          require("telescope.builtin").lsp_references()
        end,
        mode = "n",
        desc = "LSP find refferences",
      },
    },
    opts = function()
      return require "gustawxyz.configs.telescope"
    end,
    config = function(_, opts)
      local telescope = require "telescope"
      telescope.setup(opts)

      -- load extensions
      -- for _, ext in ipairs(opts.extensions_list) do
      -- telescope.load_extension(ext)
      -- end
    end,
  },
  {
    "folke/which-key.nvim",
    keys = {
      {
        "<leader>hK",
        function()
          vim.cmd "WhichKey"
        end,
        mode = "n",
        desc = "Which-key",
      },
      "<leader>",
      '"',
      "'",
      "`",
      "c",
      "v",
      "g",
    },
    config = function(_, opts)
      require("which-key").setup(opts)
    end,
  },
  {
    "nvim-neorg/neorg",
    event = "VeryLazy",
    build = ":Neorg sync-parsers",
    keys = {
      { "<leader>nn", "<cmd>Neorg index<CR>", mode = "n", desc = "Neorg index" },
      { "<leader>nr", "<cmd>Neorg return<CR>", mode = "n", desc = "Neorg return" },
    },
    dependencies = {
      { "nvim-lua/plenary.nvim" },
      { "nvim-neorg/neorg-telescope" },
      { "folke/zen-mode.nvim" },
    },
    ft = "norg",
    config = function()
      require "gustawxyz.configs.neorg"
    end,
  },
  {
    "numToStr/Comment.nvim",
    keys = {
      {
        "<leader>/",
        function()
          require("Comment.api").toggle.linewise.current()
        end,
        mode = "n",
        desc = "Toggle comment",
      },
      {
        "<leader>/",
        "<ESC><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<CR>",
        mode = "x",
        desc = "Toggle comment",
      },
      { "gcc", mode = "n", desc = "Comment toggle current line" },
      { "gc", mode = { "n", "o" }, desc = "Comment toggle linewise" },
      { "gc", mode = "x", desc = "Comment toggle linewise (visual)" },
      { "gbc", mode = "n", desc = "Comment toggle current block" },
      { "gb", mode = { "n", "o" }, desc = "Comment toggle blockwise" },
      { "gb", mode = "x", desc = "Comment toggle blockwise (visual)" },
    },
    config = function(_, opts)
      require("Comment").setup(opts)
    end,
  },
  {
    "rcarriga/nvim-dap-ui",
    event = "VeryLazy",
    dependencies = {
      "mfussenegger/nvim-dap",
      "nvim-neotest/nvim-nio"
    },
    keys = {
      {
        "<leader>dt",
        function()
          require("dapui").toggle()
        end,
        mode = "n",
        desc = "Toggle dapui",
      },
    },
    config = function()
      local dap = require "dap"
      local dapui = require "dapui"
      dapui.setup()
      dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
      end
    end,
  },
  {
    "jay-babu/mason-nvim-dap.nvim",
    event = "VeryLazy",
    dependencies = {
      "williamboman/mason.nvim",
      "mfussenegger/nvim-dap",
    },
    keys = {

      { "<leader>db", "<cmd> DapToggleBreakpoint <CR>", mode = "n", desc = "Add breakpoint at line" },
      { "<leader>dr", "<cmd> DapContinue <CR>", mode = "n", desc = "Start or continue the debugger" },
      {
        "<leader>dus",
        function()
          local widgets = require "dap.ui.widgets"
          local sidebar = widgets.sidebar(widgets.scopes)
          sidebar.open()
        end,
        mode = "n",
        desc = "Open debugging sidebar",
      },
    },
    opts = {
      handlers = {},
    },
  },
  {
    "mfussenegger/nvim-dap",
    config = function(_, _) end,
  },
  {
    "theHamsta/nvim-dap-virtual-text",
    lazy = false,
    config = function(_, _)
      require("nvim-dap-virtual-text").setup()
    end,
  },
  {
    "mfussenegger/nvim-dap-python",
    ft = "python",
    dependencies = {
      "mfussenegger/nvim-dap",
      "rcarriga/nvim-dap-ui",
      "nvim-lua/plenary.nvim",
    },
    keys = {
      {
        "<leader>dpB",
        function()
          local Job = require "plenary.job"
          vim.ui.input({ prompt = "Arguments: ", default = dap_python_arguments_cache }, function(input)
            local args = {}
            if input then
              dap_python_arguments_cache = input
              args = vim.split(input, " +")
            else
              args = vim.split(dap_python_arguments_cache, " +")
            end
            local runtime_path = Job:new({ command = "brazil-bootstrap", args = { "" } }):sync()
            for _, v in ipairs(runtime_path) do
              local path = v .. "/bin/python"
              require("dap-python").resolve_python = function()
                return path
              end
              require("dap").configurations.python = {
                {
                  type = "python",
                  request = "launch",
                  justMyCode = false,
                  name = "Launch brazil-bootstraped with Arguments",
                  program = "${file}",
                  args = args,
                  console = "integratedTerminal",
                  pythonPath = path,
                },
              }
              require("dap").continue()
            end
          end)
        end,
        mode = "n",
        desc = "Run dap with bemol python",
      },
      {
        "<leader>dpb",
        function()
          local bemol_dir = vim.fs.find({ ".bemol" }, { upward = true, type = "directory" })[1]
          local git_dir = vim.fs.find({ ".git" }, { upward = true, type = "directory" })[1]
          if git_dir then
            local project_name = vim.fs.basename(vim.fs.dirname(git_dir))
            local env_dir = vim.fs.find(function(name, _)
              return name:match(".*" .. project_name .. ".*$")
            end, { path = bemol_dir, upward = false, type = "directory" })[1]
            local path = env_dir .. "/Python/farm/bin/python3.9"
            require("dap-python").resolve_python = function()
              return path
            end
            require("dap-python").setup(path)
            require("dap").continue()
          end
        end,
        mode = "n",
        desc = "Run dap with bemol python",
      },
      {
        "<leader>dpf",
        function()
          require("dap-python").test_method()
        end,
        mode = "n",
        desc = "Run dap python",
      },
    },
    config = function()
      local path = "~/.local/share/nvim/mason/packages/debugpy/venv/bin/python"
      require("dap-python").setup(path)
    end,
  },
  -- {
  --   "nvimtools/none-ls.nvim",
  --   dependencies = {
  --     { "davidmh/cspell.nvim", event = "VeryLazy" },
  --   },
  --   event = "VeryLazy",
  --
  --   opts = function()
  --     return require "gustawxyz.configs.null-ls"
  --   end,
  -- },
  {
    "creativenull/efmls-configs-nvim",
    version = "v1.x.x", -- version is optional, but recommended
    dependencies = { "neovim/nvim-lspconfig" },
    opts = function()
      return require "gustawxyz.configs.efmls"
    end,
    config = function(_, opts)
      require("lspconfig").efm.setup(vim.tbl_extend("force", opts, {
        -- Pass your custom lsp config below like on_attach and capabilities
        --
        on_attach = opts.on_attach,
        -- capabilities = capabilities,
      }))
    end,
  },
  --   {
  --   THIS BREAKS NEOGIT
  --     "mhartington/formatter.nvim",
  --     event = "VeryLazy",
  --     opts = function()
  --       return require "gustawxyz.configs.formatter"
  --     end,
  --   },
  {
    "mfussenegger/nvim-lint",
    event = "VeryLazy",
    config = function()
      require "gustawxyz.configs.lint"
    end,
  },
  {
    "williamboman/mason.nvim",
    cmd = { "Mason", "MasonInstall", "MasonInstallAll", "MasonUninstall", "MasonUninstallAll", "MasonLog" },
    config = function()
      require "gustawxyz.configs.mason"
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    cmd = { "LspInstall", "LspUninstall" },
    config = function()
      require "gustawxyz.configs.mason-lspconfig"
    end,
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      { "nvim-telescope/telescope.nvim" },
    },
    config = function()
      require "gustawxyz.configs.lspconfig"
    end,
  },
  {
    "hrsh7th/nvim-cmp",
    url = "https://github.com/GustawXYZ/nvim-cmp",
    event = "VeryLazy",
    url = "https://github.com/GustawXYZ/nvim-cmp",
    dependencies = {
      {
        "hrsh7th/cmp-cmdline",
      },
      -- {
      --   "petertriho/cmp-git",
      --   config = function()
      --     require("cmp_git").setup()
      --   end,
      -- }
      {
        "L3MON4D3/LuaSnip",
        dependencies = "rafamadriz/friendly-snippets",
        config = function(_, opts)
          require("gustawxyz.configs.luasnip").setup(opts)
        end,
      },
      {
        "windwp/nvim-autopairs",
        opts = {
          fast_wrap = {},
          disable_filetype = { "TelescopePrompt", "vim" },
        },
        config = function(_, opts)
          require("nvim-autopairs").setup(opts)

          -- setup cmp for autopairs
          local cmp_autopairs = require "nvim-autopairs.completion.cmp"
          require("cmp").event:on("confirm_done", cmp_autopairs.on_confirm_done())
        end,
      },
      -- sources
      {
        "saadparwaiz1/cmp_luasnip",
        "hrsh7th/cmp-nvim-lua",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
      },
    },
    config = function(_, _)
      require "gustawxyz.configs.cmp"
    end,
  },
  {
    "simrat39/rust-tools.nvim",
    ft = "rust",
    dependencies = "neovim/nvim-lspconfig",
    opts = function()
      return require "gustawxyz.configs.rust-tools"
    end,
    config = function(_, opts)
      require("rust-tools").setup(opts)
    end,
  },
  {
    "rust-lang/rust.vim",
    ft = "rust",
    init = function()
      vim.g.rustfmt_autosave = 1
    end,
  },
  {
    "saecki/crates.nvim",
    ft = { "toml" },
    keys = {
      {
        "<leader>ccu",
        function()
          require("crates").update_crate()
        end,
        mode = "n",
        desc = "update crates",
      },
      {
        "<leader>ccU",
        function()
          require("crates").upgrade_all_crates()
        end,
        mode = "n",
        desc = "update crates",
      },
      {
        "<leader>cck",
        function()
          require("crates").show_popup()
        end,
        mode = "n",
        desc = "Show information about crate",
      },
    },
    config = function(_, opts)
      local crates = require "crates"
      crates.setup(opts)
      require("cmp").setup.buffer {
        sources = { { name = "crates" } },
      }
      crates.show()
    end,
  },
  {
    "dreamsofcode-io/nvim-dap-go",
    ft = "go",
    dependencies = "mfussenegger/nvim-dap",
    keys = {
      {
        "<leader>dgt",
        function()
          require("dap-go").debug_test()
        end,
        mode = "n",
        desc = "Debug go test",
      },
      {
        "<leader>dgl",
        function()
          require("dap-go").debug_last()
        end,
        mode = "n",
        desc = "Debug last go test",
      },
    },
    config = function(_, opts)
      require("dap-go").setup(opts)
    end,
  },
  {
    "olexsmir/gopher.nvim",
    ft = "go",
    keys = {
      { "<leader>gsj", "<cmd> GoTagAdd json <CR>", mode = "n", desc = "Add json struct tags" },
      { "<leader>gsy", "<cmd> GoTagAdd yaml <CR>", mode = "n", desc = "Add yaml struct tags" },
    },
    config = function(_, opts)
      require("gopher").setup(opts)
    end,
    build = function()
      vim.cmd [[silent! GoInstallDeps]]
    end,
  },
  {
    "folke/zen-mode.nvim",
    event = "VeryLazy",
    opts = function()
      return require "gustawxyz.configs.zen-mode"
    end,
  },
  -- {
  --   "romgrk/barbar.nvim",
  --   dependencies = {
  --     "lewis6991/gitsigns.nvim", -- OPTIONAL: for git status
  --     "nvim-tree/nvim-web-devicons", -- OPTIONAL: for file icons
  --   },
  --   init = function()
  --     vim.g.barbar_auto_setup = false
  --   end,
  --   opts = {
  --     icons = {
  --       gitsigns = {
  --         added = { enabled = true, icon = "+" },
  --         changed = { enabled = true, icon = "~" },
  --         deleted = { enabled = true, icon = "-" },
  --       },
  --     },
  --     sidebar_filetypes = {
  --       undotree = { text = "undotree" },
  --     },
  --   },
  --   config = function(_, opts)
  --     require("barbar").setup(opts)
  --   end,
  -- },
}

local ok, mod = pcall(require, "gustawxyz.work")
if not ok then
  mod = nil
end
if mod then
  local work_plugins = mod.plugins
  for i = 1, #work_plugins do
    plugins[#plugins + 1] = work_plugins[i]
  end
end

return plugins
