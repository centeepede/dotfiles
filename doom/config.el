;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Gustaw"
      user-mail-address "gustaw.napiorkowski@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Notes/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; org-agenda
(after! org
        (setq org-agenda-files '("~/Documents/agenda.org"))
        ;; (setq org-roam-directory "~/Documents/Logseq")
        ;; (setq org-roam-dailies-directory "journals/")
        ;; (setq org-roam-dailies-capture-templates
        ;;         '(("d" "default" entry
        ;;                 "* %?" :target
        ;;                 (file+head "%<%Y_%m_%d>.org" "#+title: %<%Y_%m_%d>\n"))))
        ;; (setq org-roam-capture-templates
        ;;         '(("d" "default" plain
        ;;                 "%?" :target
        ;;                 (file+head "pages/${slug}.org" "#+title: ${title}\n")
        ;;                 :unnarrowed t)))
)

(after! treemacs
  (setq treemacs-default-visit-action 'treemacs-visit-node-close-treemacs))

;; relative line numbering
(setq display-line-numbers 'relative)

;; (setq doom-theme 'doom-gruvbox)

;; org-auto-tangle
(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))

;; * MAPS and REMAPS
;; some general maps

(define-key global-map (kbd "C-a") 'org-increase-number-at-point)
(define-key global-map (kbd "C-S-a") 'org-decrease-number-at-point)

(map! :leader
      (:prefix ("C". "configs")
       :desc "doom"                "d" #'(lambda () (interactive) (find-file "~/.config/doom/config.el"))
       :desc "alacritty"           "a" #'(lambda () (interactive) (find-file "~/.config/alacritty/alacritty.yml"))
       :desc "qtile-keys"          "q" #'(lambda () (interactive) (find-file "~/.config/qtile/keys.py"))
       :desc "qtile-config"        "Q" #'(lambda () (interactive) (find-file "~/.config/qtile/config.py"))
       :desc "nvim"                "n" #'(lambda () (interactive) (find-file "~/.config/nvim/init.vim"))
       :desc "zsh/aliases"         "Z" #'(lambda () (interactive) (find-file "~/.zsh/aliases.zsh"))
       :desc "zshrc"               "z" #'(lambda () (interactive) (find-file "~/.zshrc"))))

(map! :leader
       :desc "vterm"            "v" #'+vterm/toggle)

;; * COMPLETION
;; company

(define-key global-map (kbd "C-.") 'company-files)

; Use tab key to cycle through suggestions.
; ('tng' means 'tab and go')
(add-hook 'after-init-hook 'company-tng-mode)

;; * Clipboard
;; Copy/past to system clipboard
(defun copy-to-clipboard ()
  "Copies selection to x-clipboard."
  (interactive)
  (if (display-graphic-p)
      (progn
        (message "Yanked region to x-clipboard.")
        (call-interactively 'clipboard-kill-ring-save)
        )
    (if (region-active-p)
        (progn
          (shell-command-on-region (region-beginning) (region-end) "pbcopy")
          (message "Yanked region to clipboard.")
          (deactivate-mark))
      (message "No region active; can't yank to clipboard!")))
  )

(defun paste-from-clipboard ()
  "Pastes from x-clipboard."
  (interactive)
  (if (display-graphic-p)
      (progn
        (clipboard-yank)
        (message "graphics active")
        )
    (insert (shell-command-to-string "pbpaste"))
    )
  )

(map! :leader
      :desc "copy-to-clipboard"
      "y y" #'copy-to-clipboard)
(map! :leader
      :desc "paste-from-clipboard"
      "y p" #'paste-from-clipboard)


;; nil = don't use system keyboard
;; (setq select-enable-clipboard nil)

;; * TRAMP

(setq tramp-default-method "rsync")

(setq tramp-terminal-type "tramp")

(setq tramp-ssh-controlmaster-options "")


(with-eval-after-load "tramp"
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path))

(map! :map evil-normal-state-map
      "K" #'lsp-ui-doc-show)

(after! lsp-mode
;;   (set-lsp-priority! 'clangd 1)  ; ccls has priority 0
;;   )

;; lsp-mode over tramp

;; (with-eval-after-load 'lsp-mode
(lsp-register-client
    (make-lsp-client :new-connection (lsp-tramp-connection "clangd")
                     :major-modes '(c++-mode cc-mode)
                     :remote? t
                     :server-id 'clangd-remote))
  ;; (lsp-register-client
  ;;  (make-lsp-client
  ;;   :new-connection (lsp-tramp-connection (cons "pyright-langserver" lsp-pyright-langserver-command-args))
  ;;   :major-modes '(python-mode)
  ;;   :remote? t
  ;;   :server-id 'pyright-remote
  ;;   :multi-root t
  ;;   ))
  ;; (lsp-register-client
  ;;  (make-lsp-client
  ;;   :new-connection (lsp-tramp-connection (cons "pyright-langserver" lsp-pyright-langserver-command-args))
  ;;   :major-modes '(python-mode)
  ;;   :remote? t
  ;;   :server-id 'pyright-remote
  ;;   :multi-root t
  ;;   :priority 3
  ;;   :initialized-fn (lambda (workspace)
  ;;                     (with-lsp-workspace workspace
  ;;       		;; we send empty settings initially, LSP server will ask for the
  ;;       		;; configuration of each workspace folder later separately
  ;;       		(lsp--set-configuration
  ;;       		 (make-hash-table :test 'equal))))
  ;;   :download-server-fn (lambda (_client callback error-callback _update?)
  ;;                         (lsp-package-ensure 'pyright callback error-callback))
  ;;   :notification-handlers (lsp-ht ("pyright/beginProgress" 'lsp-pyright--begin-progress-callback)
  ;;                                  ("pyright/reportProgress" 'lsp-pyright--report-progress-callback)
  ;;                                  ("pyright/endProgress" 'lsp-pyright--end-progress-callback))))
  ;;   ))
)
(setq enable-remote-dir-locals t)
(setq enable-local-variables :all)

(after! lsp-pyright
  (setq lsp-log-io t)
  (setq lsp-pyright-use-library-code-for-types t)
  (setq lsp-pyright-diagnostic-mode "workspace")
  (lsp-register-client
    (make-lsp-client
      :new-connection (lsp-tramp-connection (lambda ()
                                      (cons "pyright-langserver"
                                            lsp-pyright-langserver-command-args)))
      :major-modes '(python-mode)
      :remote? t
      :server-id 'pyright-remote
      :multi-root t
      :priority 3
      :initialization-options (lambda () (ht-merge (lsp-configuration-section "pyright")
                                                   (lsp-configuration-section "python")))
      :initialized-fn (lambda (workspace)
                        (with-lsp-workspace workspace
                          (lsp--set-configuration
                          (ht-merge (lsp-configuration-section "pyright")
                                    (lsp-configuration-section "python")))))
      :download-server-fn (lambda (_client callback error-callback _update?)
                            (lsp-package-ensure 'pyright callback error-callback))
      :notification-handlers (lsp-ht ("pyright/beginProgress" 'lsp-pyright--begin-progress-callback)
                                    ("pyright/reportProgress" 'lsp-pyright--report-progress-callback)
                                    ("pyright/endProgress" 'lsp-pyright--end-progress-callback))))
  )

;; * VTERM

(setq vterm-copy-exclude-prompt t)
(setq vterm-max-scrollback 100000)
(setq vterm-tramp-shells '(("ssh" "/bin/zsh")
                        ("sshx" "/bin/zsh")
                        ("rsync" "/bin/zsh")))

;; (add-to-list 'tramp-connection-properties
;;              (list (regexp-quote "/ssh:217.182.78.181@debian:")
;;                    "remote-shell" "/usr/bin/zsh"))


;; * DIRED

;; Get file icons in dired
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

;; previews
;;
(map! :leader
        (:prefix ("d" . "dired")
                :desc "Open dired" "d" #'dired
                :desc "Dired jump to current" "j" #'dired-jump)
        (:after dired
                (:map dired-mode-map
                        :desc "Peep-dired image previews" "d p" #'peep-dired
                        :desc "Dired view file"           "d v" #'dired-view-file)))

(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))

(evil-define-key 'normal peep-dired-mode-map
        (kbd "j") 'peep-dired-next-file
        (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

(setq ranger-override-dired-mode t)

;; * MACOS
(when (eq system-type 'darwin)
        (setq mac-option-modifier 'meta))

;; *PYTHON
;; (setq lsp-pyls-server-command '("br-pyls"))

(setq pyenv-installation-dir "$HOME/.pyenv")
(setq pyenv-executable "$HOME/.pyenv/bin/pyenv")
(setq pyenv-python-shim "$HOME/.pyenv/shims/python")
(setq pyenv-global-version-file "$HOME/.pyenv/version")
(global-pyenv-mode)

(setq flycheck-checker-error-threshold 10000)

(add-hook 'sonic-pi-mode-hook
          (lambda ()
            ;; This setq can go here instead if you wish
            (setq sonic-pi-path "/usr/lib/sonic-pi")
            (define-key ruby-mode-map "\C-c\C-b" 'sonic-pi-stop-all)))

(if IS-MAC (setq ns-use-native-fullscreen t))
(if IS-MAC (add-to-list 'default-frame-alist '(fullscreen . fullboth)))

;; make _ be a part of wards
(modify-syntax-entry ?_ "w")
;; For python
;; (add-hook! 'python-mode-hook (modify-syntax-entry ?_ "w"))
;; for executable of language server, if it's not symlinked on your PATH
;; (setq lsp-python-ms-executable
;;       "~/python-language-server/output/bin/Release/osx-x64/publish/Microsoft.Python.LanguageServer")

